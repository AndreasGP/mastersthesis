﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(SaveableTransform))]
public class SaveableTransformEditor : Editor
{
    
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SaveableTransform myScript = (SaveableTransform)target;
        if (GUILayout.Button("Save"))
        {
            myScript.Save();
        }
        if (GUILayout.Button("Load"))
        {
            myScript.Load();
        }
    }
}
#endif
