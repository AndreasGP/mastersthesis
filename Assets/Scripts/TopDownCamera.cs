﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour
{

    public float MinZoom = 10;

    public int MaxZoom = 2000;

    public float ScrollRate = 1.5f;

	void Start ()
	{
	}
	
	void Update () {
        var height = transform.position.y;

        if (Input.GetMouseButton(0))
	    {
	        var speed = 1.8f;
	        var deltaX = -Input.GetAxis("Mouse X") * speed * Time.deltaTime * height;
	        var deltaZ = -Input.GetAxis("Mouse Y") * speed * Time.deltaTime * height;
	        transform.position += new Vector3(deltaX, 0, deltaZ);
	    }

	    var zoom = Input.GetAxis("Mouse ScrollWheel");
	    var p = transform.position;
        p.y += -zoom * height * ScrollRate;
	    p.y = Mathf.Clamp(p.y, MinZoom, MaxZoom);
	    transform.position = p;


	}
}
