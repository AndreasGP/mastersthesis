﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{

    public Settings Settings;

    public GameObject ControlPanelFull;

    public GameObject ControlPanelSmall;

    public GameObject TeleportPanelFull;

    public GameObject TeleportPanelSmall;

    public GameObject CarParent;

    public Camera CarCamera;

    public Camera FlyCamera;

    public Text TerrainObjectSpawnRadiusText;

    public Slider TerrainObjectSpawnRadiusSlider;

    public Text MesoChunkGenerationRadiusText;

    public Slider MesoChunkGenerationRadiusSlider;

    public Text MinFramesForChunkText;

    public Slider MinFramesForChunkSlider;

    public Text MaxObjectsPerFrameText;

    public Slider MaxObjectsPerFrameSlider;

    public Toggle FlightCameraToggle;

    public Toggle CarCameraToggle;

    public Button ResetCarButton;
    
    public Button TeleportCityHighwayButton;

    public Button TeleportCitySuburbButton;

    public Button TeleportVillageButton;
    
    public Button TeleportRuralRoadJunctionButton;
    
    public Button TeleportForestButton;

    public Button TelepotrFarmButton;

    public Button TeleportNatureReserveButton;

    void Start () {

        TerrainObjectSpawnRadiusSlider.onValueChanged.AddListener(val =>
        {
            TerrainObjectSpawnRadiusText.text = "Terrain Object Spawn Radius (" + (int)val + " m):";
            Settings.terrainObjectSpawnRadius = (int)val;
        });

        MesoChunkGenerationRadiusSlider.onValueChanged.AddListener(val =>
        {
            MesoChunkGenerationRadiusText.text = "Mesochunk Generation Radius (" + (int)val + "):";
            Settings.mesoGenerationRadius = (int)val;
        });

        MinFramesForChunkSlider.onValueChanged.AddListener(val =>
        {
            MinFramesForChunkText.text = "Min Frames per Instantiating a Chunk (" + (int)val + "):";
            Settings.minFramesPerChunk = (int)val;
        });

        MaxObjectsPerFrameSlider.onValueChanged.AddListener(val =>
        {
            MaxObjectsPerFrameText.text = "Max Objects Instantiated per Frame (" + (int)val + "):";
            Settings.maxObjectsInstantiatedPerFrame = (int)val;
        });

        FlightCameraToggle.onValueChanged.AddListener(val => {
            if (val)
            {
                //Activating flight camera
                FlyCamera.transform.position = CarCamera.transform.position + new Vector3(0, 5, 0);
                FlyCamera.transform.rotation = CarCamera.transform.rotation;
                FlyCamera.gameObject.SetActive(true);
                ChunkManagerWrapper.instance.SetWorldCenter(FlyCamera.transform);
                CarParent.gameObject.SetActive(false);
            }

        });

        CarCameraToggle.onValueChanged.AddListener(val => {
            if (val)
            {
                //Activating car camera
                CarParent.transform.position = FlyCamera.transform.position;
                CarParent.transform.rotation = FlyCamera.transform.rotation;
                CarParent.gameObject.SetActive(true);
                ChunkManagerWrapper.instance.SetWorldCenter(CarParent.transform);
                FlyCamera.gameObject.SetActive(false);
            }
        });

        ResetCarButton.onClick.AddListener(() =>
        {
            if (CarParent.gameObject.activeSelf)
            {
                CarParent.transform.rotation = Quaternion.identity;
                CarParent.transform.position = CarParent.transform.position + new Vector3(0, 5, 0);
            }
        }
            );


    TeleportCityHighwayButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(-4016, 500, 4128));
    });

    TeleportCitySuburbButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(-3597, 500, 1793));
    });

    TeleportVillageButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(-267, 450, 5900));
    });

    TeleportRuralRoadJunctionButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(215, 450, 5401));
    });

    TeleportForestButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(1113, 500, 5464));
    });

    TelepotrFarmButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(3440, 280, 5820));
    });

    TeleportNatureReserveButton.onClick.AddListener(() =>
    {
        Teleport(new Vector3(-7155, 450, -1279));
    });
    }

    private void Teleport(Vector3 pos)
    {
        if (CarParent.gameObject.activeSelf)
        {
            CarParent.transform.position = pos;
        }
        else
        {
            FlyCamera.transform.position = pos;
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (ControlPanelFull.activeSelf)
            {
                ControlPanelFull.SetActive(false);
                ControlPanelSmall.SetActive(true);
            }
            else
            {
                ControlPanelFull.SetActive(true);
                ControlPanelSmall.SetActive(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            if (TeleportPanelFull.activeSelf)
            {
                TeleportPanelFull.SetActive(false);
                TeleportPanelSmall.SetActive(true);
            }
            else
            {
                TeleportPanelFull.SetActive(true);
                TeleportPanelSmall.SetActive(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (FlyCamera.gameObject.activeSelf)
            {
//                var camScript = FlyCamera.gameObject.GetComponent<GhostFreeRoamCamera>();
//                if (camScript != null)
//                {
//                    camScript.enabled = !camScript.enabled;
//                }
            }
        }
    }

}
