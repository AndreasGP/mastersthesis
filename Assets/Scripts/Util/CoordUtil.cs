﻿using System;
using UnityEngine;

public static class CoordUtil {


    #region position checks
    public static bool OnMacroChunk(Vector3 pos, Point2 macroChunkPos)
    {
        return OnMacroChunk(pos, macroChunkPos, 0);
    }

    public static bool OnMacroChunk(Vector3 pos, Point2 macroChunkPos, float maxOffset)
    {
        var min = MacroChunkToGlobalPosition(macroChunkPos);
        var w = Const.MACRO_CHUNK_WIDTH_IN_METERS;
        return pos.x >= min.x - maxOffset && pos.x < min.x + w + maxOffset && pos.z >= min.z - maxOffset && pos.z < min.z + w + maxOffset;
    }

    #endregion

    #region macro chunk transforms
    public static Vector3 MacroChunkToGlobalPosition(Point2 macroChunkPos)
    {
        return MacroChunkToGlobalPosition(macroChunkPos.x, macroChunkPos.z);
    }

    public static Vector3 MacroChunkToGlobalPosition(int macroChunkX, int macroChunkZ)
    {
        var x = Const.MACRO_CHUNK_WIDTH_IN_METERS * macroChunkX;
        var z = Const.MACRO_CHUNK_WIDTH_IN_METERS * macroChunkZ;
        return new Vector3(x, 0, z);
    }

    public static Point2 MacroChunkToGlobalTile(Point2 macroChunkPos) {
        return MacroChunkToGlobalTile(macroChunkPos.x, macroChunkPos.z);
    }

    public static Point2 MacroChunkToGlobalTile(int macroChunkX, int macroChunkZ) {
        var x = Const.MACRO_CHUNK_WIDTH_IN_TILES * macroChunkX;
        var z = Const.MACRO_CHUNK_WIDTH_IN_TILES * macroChunkZ;
        return new Point2(x, z);
    }

    public static Point2 MacroChunkToMicroChunk(Point2 macroChunkPos)
    {
        return MacroChunkToMicroChunk(macroChunkPos.x, macroChunkPos.z);
    }

    public static Point2 MacroChunkToMicroChunk(int macroChunkX, int macroChunkZ)
    {
        var x = Const.MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS * macroChunkX;
        var z = Const.MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS * macroChunkZ;
        return new Point2(x, z);
    }

    public static Point2 MacroChunkToMesoChunk(Point2 macroChunkPos) {
        return MacroChunkToMesoChunk(macroChunkPos.x, macroChunkPos.z);
    }

    public static Point2 MacroChunkToMesoChunk(int macroChunkX, int macroChunkZ) {
        var x = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS * macroChunkX;
        var z = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS * macroChunkZ;
        return new Point2(x, z);
    }
    #endregion

    #region meso chunk transforms
    public static Vector3 MesoChunkToGlobalPosition(Point2 mesoChunkPos)
    {
        return MesoChunkToGlobalPosition(mesoChunkPos.x, mesoChunkPos.z);
    }

    public static Vector3 MesoChunkToGlobalPosition(int mesoChunkX, int mesoChunkZ)
    {
        var x = Const.MESO_CHUNK_WIDTH_IN_METERS * mesoChunkX;
        var z = Const.MESO_CHUNK_WIDTH_IN_METERS * mesoChunkZ;
        return new Vector3(x, 0, z);
    }

    public static Point2 MesoChunkToMacroChunk(Point2 mesoChunkPos)
    {
        return MesoChunkToMacroChunk(mesoChunkPos.x, mesoChunkPos.z);
    }

    public static Point2 MesoChunkToMacroChunk(int mesoChunkX, int mesoChunkZ)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;
        int _x = mesoChunkX >= 0 ? mesoChunkX / w : (mesoChunkX - w + 1) / w;
        int _z = mesoChunkZ >= 0 ? mesoChunkZ / w : (mesoChunkZ - w + 1) / w;
        return new Point2(_x, _z);
    }

    public static Point2 GlobalMesoChunkToLocalMesoChunk(Point2 globalChunk) {
        return GlobalMesoChunkToLocalMesoChunk(globalChunk.x, globalChunk.z);
    }

    public static Point2 GlobalMesoChunkToLocalMesoChunk(int x, int z)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;
        int _x = (x >= 0 ? x % w : w - 1 - (-x - 1) % w);
        int _z = (z >= 0 ? z % w: w - 1 - (-z - 1) % w);
        return new Point2(_x, _z);
    }

    public static bool IsLocalMesoChunkOnMacroChunk(Point2 localMesoChunk) {
        return IsLocalMesoChunkOnMacroChunk(localMesoChunk.x, localMesoChunk.z);
    }

    public static bool IsLocalMesoChunkOnMacroChunk(int x, int z) {
        return x >= 0 && z >= 0 && x < Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS && z < Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;
    }
    #endregion

    #region micro chunk transforms
    public static Point2 MicroChunkToMacroChunk(Point2 microChunkPos)
    {
        return MicroChunkToMacroChunk(microChunkPos.x, microChunkPos.z);
    }

    public static Point2 MicroChunkToMacroChunk(int microChunkX, int microChunkZ)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS;
        int _x = microChunkX >= 0 ? microChunkX / w : (microChunkX - w + 1) / w;
        int _z = microChunkZ >= 0 ? microChunkZ / w : (microChunkZ - w + 1) / w;
        return new Point2(_x, _z);
    }

    public static Point2 MicroChunkToMesoChunk(Point2 microChunkPos)
    {
        return MicroChunkToMesoChunk(microChunkPos.x, microChunkPos.z);
    }

    public static Point2 MicroChunkToMesoChunk(int microChunkX, int microChunkZ)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_MICRO_CHUNKS;
        int _x = microChunkX >= 0 ? microChunkX / w : (microChunkX - w + 1) / w;
        int _z = microChunkZ >= 0 ? microChunkZ / w : (microChunkZ - w + 1) / w;
        return new Point2(_x, _z);
    }

    public static Vector3 MicroChunkToGlobalPosition(Point2 microChunkPos)
    {
        return MicroChunkToGlobalPosition(microChunkPos.x, microChunkPos.z);
    }

    public static Vector3 MicroChunkToGlobalPosition(int microChunkX, int microChunkZ)
    {
        float x = Const.MICRO_CHUNK_WIDTH_IN_METERS * microChunkX;
        float z = Const.MICRO_CHUNK_WIDTH_IN_METERS * microChunkZ;
        return new Vector3(x, 0, z);
    }

    #endregion


    #region tile transforms
    public static Vector3 GlobalTileToGlobalPosition(Point2 globalTile)
    {
        return GlobalTileToGlobalPosition(globalTile.x, globalTile.z);
    }

    public static Vector3 GlobalTileToGlobalPosition(int globalTileX, int globalTileZ)
    {
        float x = Const.TILE_WIDTH_IN_METERS * globalTileX;
        float z = Const.TILE_WIDTH_IN_METERS * globalTileZ;
        return new Vector3(x, 0, z);
    }

    public static Point2 GlobalTileToMacroChunk(Point2 globalTile) {
        return GlobalTileToMacroChunk(globalTile.x, globalTile.z);
    }

    public static Point2 GlobalTileToMacroChunk(int x, int z)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_TILES;
        int _x = x >= 0 ? x / w : (x - w + 1) / w;
        int _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2(_x, _z);
    }

    public static Point2 GlobalTileToMesoChunk(Point2 globalTile)
    {
        return GlobalTileToMesoChunk(globalTile.x, globalTile.z);
    }

    public static Point2 GlobalTileToMesoChunk(int x, int z)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_TILES;
        int _x = x >= 0 ? x / w : (x - w + 1) / w;
        int _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2(_x, _z);
    }

    public static Point2 GlobalTileToMicroChunk(Point2 globalTile)
    {
        return GlobalTileToMicroChunk(globalTile.x, globalTile.z);
    }

    public static Point2 GlobalTileToMicroChunk(int x, int z)
    {
        var w = Const.MICRO_CHUNK_WIDTH_IN_TILES;
        int _x = x >= 0 ? x / w : (x - w + 1) / w;
        int _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2(_x, _z);
    }


    public static Point2 MacroChunkGlobalTileToLocalTile(Point2 globalTile)
    {
        return MacroChunkGlobalTileToLocalTile(globalTile.x, globalTile.z);
    }

    public static Point2 MacroChunkGlobalTileToLocalTile(int x, int z)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_TILES;
        int _x = x >= 0 ? x % w : w - 1 - (-x - 1) % w;
        int _z = z >= 0 ? z % w : w - 1 - (-z - 1) % w;
        return new Point2(_x, _z);
    }

    public static Point2 MesoChunkGlobalTileToLocalTile(Point2 globalTile)
    {
        return MesoChunkGlobalTileToLocalTile(globalTile.x, globalTile.z);
    }

    public static Point2 MesoChunkGlobalTileToLocalTile(int x, int z)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_TILES;
        int _x = x >= 0 ? x % w : w - 1 - (-x - 1) % w;
        int _z = z >= 0 ? z % w : w - 1 - (-z - 1) % w;
        return new Point2(_x, _z);
    }
    #endregion

    #region position transforms
    public static Point2 GlobalPositionToMacroChunk(Vector3 pos) {
        return GlobalPositionToMacroChunk(pos.x, pos.z);
    }

    public static Point2 GlobalPositionToMacroChunk(float x, float z)
    {
        var w = Const.MACRO_CHUNK_WIDTH_IN_METERS;
        float _x = x >= 0 ? x / w : (x - w + 1) / w;
        float _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2((int)_x, (int)_z);
    }

    public static Point2 GlobalPositionToMesoChunk(Vector3 pos)
    {
        return GlobalPositionToMesoChunk(pos.x, pos.z);
    }

    public static Point2 GlobalPositionToMesoChunk(float x, float z)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_METERS;
        float _x = x >= 0 ? x / w : (x - w + 1) / w;
        float _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2((int)_x, (int)_z);
    }

    public static Point2 GlobalPositionToMicroChunk(Vector3 pos)
    {
        return GlobalPositionToMicroChunk(pos.x, pos.z);
    }

    public static Point2 GlobalPositionToMicroChunk(float x, float z)
    {
        var w = Const.MICRO_CHUNK_WIDTH_IN_METERS;
        float _x = x >= 0 ? x / w : (x - w + 1) / w;
        float _z = z >= 0 ? z / w : (z - w + 1) / w;
        return new Point2((int)_x, (int)_z);
    }

    public static Vector3 GlobalPositionToLocalMesoPosition(Vector3 globalPos)
    {
        return GlobalPositionToLocalMesoPosition(globalPos.x, globalPos.z);
    }

    public static Vector3 GlobalPositionToLocalMesoPosition(float x, float z)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_METERS;
        var _x = (x >= 0 ? x % w : w - 1 - (-x - 1) % w);
        var _z = (z >= 0 ? z % w : w - 1 - (-z - 1) % w);
        return new Vector3(_x, 0, _z);
    }

    #endregion

    //old stuff

    #region Chunk transformations
    public static Vector3 ChunkToGlobalPosition(Point2 chunkPos) {
        return ChunkToGlobalPosition(chunkPos.x, chunkPos.z);
    }

    public static Vector3 ChunkToGlobalPosition(int chunkX, int chunkZ) {
        float x = Const.CHUNK_WIDTH_IN_METERS * chunkX;
        float z = Const.CHUNK_WIDTH_IN_METERS * chunkZ;
        return new Vector3(x, 0, z);
    }

    public static Point2 GlobalChunkToLocalChunk(Point2 globalChunk) {
        return GlobalChunkToLocalChunk(globalChunk.x, globalChunk.z);
    }

    public static Point2 GlobalChunkToLocalChunk(int x, int z) {
        int _x = (x >= 0 ? x % Const.RIVER_CHUNK_WIDTH_IN_CHUNKS : Const.RIVER_CHUNK_WIDTH_IN_CHUNKS - 1 - (-x - 1) % Const.RIVER_CHUNK_WIDTH_IN_CHUNKS);
        int _z = (z >= 0 ? z % Const.RIVER_CHUNK_WIDTH_IN_CHUNKS : Const.RIVER_CHUNK_WIDTH_IN_CHUNKS - 1 - (-z - 1) % Const.RIVER_CHUNK_WIDTH_IN_CHUNKS);
        return new Point2(_x, _z);
    }

    public static Point2 ChunkToRiverChunk(Point2 chunk) {
        return ChunkToRiverChunk(chunk.x, chunk.z);
    }

    public static Point2 ChunkToRiverChunk(int x, int z) {
        int _x = x >= 0 ? x / Const.RIVER_CHUNK_WIDTH_IN_CHUNKS : (x - Const.RIVER_CHUNK_WIDTH_IN_CHUNKS + 1) / Const.RIVER_CHUNK_WIDTH_IN_CHUNKS;
        int _z = z >= 0 ? z / Const.RIVER_CHUNK_WIDTH_IN_CHUNKS : (z - Const.RIVER_CHUNK_WIDTH_IN_CHUNKS + 1) / Const.RIVER_CHUNK_WIDTH_IN_CHUNKS;
        return new Point2(_x, _z);
    }
    #endregion


    #region Position transformations
    public static Point2 GlobalPositionToChunk(Vector3 pos) {
        return GlobalPositionToChunk(pos.x, pos.z);
    }

    public static Point2 GlobalPositionToChunk(float x, float z) {
        float _x = x >= 0 ? x / Const.CHUNK_WIDTH_IN_METERS : (x - Const.CHUNK_WIDTH_IN_METERS + 1) / Const.CHUNK_WIDTH_IN_METERS;
        float _z = z >= 0 ? z / Const.CHUNK_WIDTH_IN_METERS : (z - Const.CHUNK_WIDTH_IN_METERS + 1) / Const.CHUNK_WIDTH_IN_METERS;
        return new Point2((int)_x, (int)_z);
    }

    public static Point2 GlobalPositionToGlobalTile(Vector3 pos) {
        return GlobalPositionToGlobalTile(pos.x, pos.z);
    }

    public static Point2 GlobalPositionToGlobalTile(Vector2 pos) {
        return GlobalPositionToGlobalTile(pos.x, pos.y);
    }

    public static Point2 GlobalPositionToGlobalTile(float x, float z) {
        int _x = x >= 0 ? (int)(x / Const.TILE_WIDTH_IN_METERS) : (int)(x / Const.TILE_WIDTH_IN_METERS) - 1;
        int _z = z >= 0 ? (int)(z / Const.TILE_WIDTH_IN_METERS) : (int)(z / Const.TILE_WIDTH_IN_METERS) - 1;
        return new Point2(_x, _z);
    }
    #endregion

    #region River Chunk transformations
    public static Vector2 RiverChunkToGlobalPosition(Point2 chunkPos) {
        return RiverChunkToGlobalPosition(chunkPos.x, chunkPos.z);
    }

    public static Vector2 RiverChunkToGlobalPosition(int chunkX, int chunkZ) {
        float x = Const.RIVER_CHUNK_WIDTH_IN_CHUNKS * Const.CHUNK_WIDTH_IN_METERS * chunkX;
        float y = Const.RIVER_CHUNK_WIDTH_IN_CHUNKS * Const.CHUNK_WIDTH_IN_METERS * chunkZ;
        return new Vector3(x, y);
    }

    #endregion

    #region Index transformations
    public static int xzToI(Point2 point) {
        return xzToI(point.x, point.z);
    }

    public static int xzToI(int x, int z) {
        return z * Const.CHUNK_WIDTH_IN_TILES + x;
    }

    public static int xzToI_vertices(int x, int z) {
        return z * Const.CHUNK_WIDTH_IN_VERTICES + x;
    }
    #endregion
}
