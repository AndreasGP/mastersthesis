﻿using System.Collections;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public static class ClipperUtil
{
    public static float COORD_MULTIPLIER = 50;

    public static List<IntPoint> ToIntPoints(List<Vector3> values)
    {
        var points = new List<IntPoint>();
        for (int i = 0; i < values.Count; i++) {
            points.Add(new IntPoint((int)(values[i].x * COORD_MULTIPLIER), (int)(values[i].z * COORD_MULTIPLIER)));
        }
        return points;
    }

    public static List<Vector3> ToVectors(List<IntPoint> points) {
        var vectors = new List<Vector3>();
        for (int i = 0; i < points.Count; i++) {
            vectors.Add(new Vector3(points[i].X / COORD_MULTIPLIER, 0, points[i].Y / COORD_MULTIPLIER));
        }
        return vectors;
    }

}
