﻿using System;
using UnityEngine;

public class FPSDisplay : MonoBehaviour {
    float deltaTime = 0.0f;

    private float minFps = 1000;

    private float maxFps = 0;

    private float avgFps = 0;

    private int frameCount = 0;

    void Update() {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;

        float fps = 1.0f / deltaTime;
        frameCount++;

        if (fps < minFps)
            minFps = fps;

        if (fps > maxFps)
            maxFps = fps;

        avgFps = (avgFps * frameCount + fps) / (frameCount + 1);


        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetCounter();
        }

    }

    private void ResetCounter()
    {
        float fps = 1.0f / deltaTime;

        avgFps = fps;
        minFps = fps;
        maxFps = fps;
        frameCount = 1;
    }

    void OnGUI() {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        var rect = new Rect(0, 0, 150, 90);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(1.0f, 1.0f, 1f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;


//        GUI.Box(rect, ".");

        string currentFpsText = string.Format("Cur: {0:0.0} ms ({1:0.} fps)", msec, fps);
        Rect currentFpsRect = new Rect(0, 0, h * 2 / 10, h * 2 / 100);
        GUI.Label(currentFpsRect, currentFpsText, style);

        Rect infoRect = new Rect(0, 18, h * 2 / 10, h * 2 / 100);
        GUI.Label(infoRect, "Press R to restart counter", style);


        string minFpsText = string.Format("Min: {0:0.} fps", minFps);
        Rect minFpsRect = new Rect(0, 36, h * 2 / 10, h * 2 / 100);
        GUI.Label(minFpsRect, minFpsText, style);

        string avgFpsText = string.Format("Avg: {0:0.} fps", avgFps);
        Rect avgFpsRect = new Rect(0, 54, h * 2 / 10, h * 2 / 100);
        GUI.Label(avgFpsRect, avgFpsText, style);

        string maxFpsText = string.Format("Max: {0:0.} fps", maxFps);
        Rect maxFpsRect = new Rect(0, 72, h * 2 / 10, h * 2 / 100);
        GUI.Label(maxFpsRect, maxFpsText, style);
    }

}