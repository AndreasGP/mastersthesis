﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveableTransform : MonoBehaviour {

    public void Save()
    {
        PlayerPrefs.SetFloat("posX", transform.position.x);
        PlayerPrefs.SetFloat("posY", transform.position.y);
        PlayerPrefs.SetFloat("posZ", transform.position.z);
        PlayerPrefs.SetFloat("rotX", transform.rotation.x);
        PlayerPrefs.SetFloat("rotY", transform.rotation.y);
        PlayerPrefs.SetFloat("rotZ", transform.rotation.z);
        PlayerPrefs.SetFloat("rotW", transform.rotation.w);
    }

    public void Load()
    {
        transform.position = new Vector3(PlayerPrefs.GetFloat("posX"), PlayerPrefs.GetFloat("posY"),
            PlayerPrefs.GetFloat("posZ"));
        transform.rotation = new Quaternion(PlayerPrefs.GetFloat("rotX"), PlayerPrefs.GetFloat("rotY"), PlayerPrefs.GetFloat("rotZ"), PlayerPrefs.GetFloat("rotW"));
    }
}
