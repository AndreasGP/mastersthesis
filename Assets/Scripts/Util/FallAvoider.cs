﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallAvoider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	    if (transform.position.y < -5)
	    {
	        var pos = transform.position;
	        pos.y = MesoChunk.GetHeightAtS(transform.position) + 5;
	        transform.position = pos;
	    }
        
	}
}
