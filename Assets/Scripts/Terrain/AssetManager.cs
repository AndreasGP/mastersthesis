﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AssetManager : MonoBehaviour
{

    public static AssetManager Ins;

    public GameObject GrassPrefab;

    public GameObject MaplePrefab;

    public GameObject SprucePrefab;

    public GameObject BushPrefab;

    public GameObject BigFarmPlantPrefab;

    public GameObject SmallFarmPlantPrefab;
    
    public GameObject UtilityPolePrefab;

    public Material UtilityPoleLineMaterial;

    public GameObject LowFencePrefab;

    public GameObject HighFencePrefab;

    public List<GameObject> HousePrefabs;

    public GameObject ChurchPrefab;

    private Dictionary<RegionType, TerrainObjectSamplerConfig> samplerConfigs;

    public List<Material> RoadMaterials;

    #region cities
    public List<Material> CityMaterials;
    
    public List<int> NumberOfFloorsOnCityMaterials;
    #endregion

    #region traffic signs
    public GameObject SettlementAheadSignPrefab;

    public GameObject SettlementLeftSignPrefab;

    public GameObject SettlementRightSignPrefab;

    public GameObject SettlementBeginSignPrefab;
   
    public GameObject SettlementEndSignPrefab;

    public GameObject SpeedLimit30SignPrefab;

    public GameObject SpeedLimit50SignPrefab;

    public GameObject SpeedLimit70SignPrefab;

    public GameObject SpeedLimit90SignPrefab;

    public GameObject SpeedLimit110SignPrefab;
    #endregion

    public int maxCityMaterialIndex;

    void Start ()
	{
	    Ins = this;

	    GenerateBillboard(GrassPrefab.GetComponent<Billboardable>());
        GenerateBillboard(BushPrefab.GetComponent<Billboardable>());
	    GenerateBillboard(BigFarmPlantPrefab.GetComponent<Billboardable>());
	    GenerateBillboard(SmallFarmPlantPrefab.GetComponent<Billboardable>());

        GenerateTerrainObjectConfigs();

	    maxCityMaterialIndex = CityMaterials.Count;
	}

    private void GenerateBillboard(Billboardable billboaradable)
    {
        var billboardAsset = BillboardManager.GetOrGenerateBillboardAsset(billboaradable.BillboardMaterial, billboaradable.Width, billboaradable.Height, billboaradable.Bottom);
        billboaradable.BillboardRenderer.billboard = billboardAsset;
    }


    private void GenerateTerrainObjectConfigs()
    {
        samplerConfigs = new Dictionary<RegionType, TerrainObjectSamplerConfig>();

        //Inner dictionary key is the probability of the specific object
        samplerConfigs.Add(RegionType.SubConiferousForestNatural, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 8f, 8f, 0f, 0f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(SprucePrefab, 0.98f, 0.65f, 0.3f, 6f),
                new TerrainObjectConfig(MaplePrefab, 0.02f, 0.9f, 0.45f, 0f)
            },
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f)
            }));

        samplerConfigs.Add(RegionType.SubConiferousForestPlanted, new TerrainObjectSamplerConfig(SamplerType.Grid, 7f, 7f, 1f, 1f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(SprucePrefab, 1f, 0.7f, 0.3f, 6f)
            },
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f)
            }
        ));

        samplerConfigs.Add(RegionType.SubDeciduousForestNatural, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 6.5f, 6.5f, 0f, 0f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 0.98f, 0.9f, 0.45f, 0f),
                new TerrainObjectConfig(SprucePrefab, 0.02f, 0.65f, 0.3f, 6f)
            },
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f)
            }));

        samplerConfigs.Add(RegionType.SubDeciduousForestPlanted, new TerrainObjectSamplerConfig(SamplerType.Grid, 6f, 6f, 1f, 1f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 1f, 0.9f, 0.45f, 0f)
            }, 
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f)
            }
            ));

        samplerConfigs.Add(RegionType.SubMixedForest, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 6.5f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 0.5f, 0.75f, 0.4f, 0f),
                new TerrainObjectConfig(SprucePrefab, 0.5f, 0.7f, 0.25f, 6f)
            }));

        samplerConfigs.Add(RegionType.SubFarmBig, new TerrainObjectSamplerConfig(SamplerType.Grid, 2f, 2.75f, false,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BigFarmPlantPrefab, 1f, 1.2f, 0.2f, 0f)
            }));

        samplerConfigs.Add(RegionType.SubFarmSmall, new TerrainObjectSamplerConfig(SamplerType.Grid, 1.5f, 2.25f, false,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(SmallFarmPlantPrefab, 1f, 1.1f, 0.25f, 0f)
            }));

        samplerConfigs.Add(RegionType.SubFarmEmpty, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 15f, false,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(SmallFarmPlantPrefab, 1f, 0.9f, 0.2f, 0f)
            }));

        samplerConfigs.Add(RegionType.SubResidenceWithTrees, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 10f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 0.5f, 0.75f, 0.4f, 0f),
                new TerrainObjectConfig(SprucePrefab, 0.5f, 0.7f, 0.25f, 6f)
            }));

        samplerConfigs.Add(RegionType.SubResidenceWithTreesAndBushes, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 8f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 0.25f, 0.75f, 0.4f, 0f),
                new TerrainObjectConfig(SprucePrefab, 0.25f, 0.7f, 0.25f, 6f),
                new TerrainObjectConfig(BushPrefab, 0.5f, 0.7f, 0.7f, 0f)
            }));
        samplerConfigs.Add(RegionType.SubResidenceWithBushes, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 8f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 0.95f, 0.7f, 0.7f, 0f),
                new TerrainObjectConfig(MaplePrefab, 0.05f, 0.75f, 0.4f, 0f),
//                new TerrainObjectConfig(GrassPrefab, 0.5f, 0.75f, 0.4f, 0f),
            }));
        samplerConfigs.Add(RegionType.SubResidenceWithSparsePlants, new TerrainObjectSamplerConfig(SamplerType.PoissonDisc, 15f, true,
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(MaplePrefab, 0.25f, 0.75f, 0.4f, 0f),
                new TerrainObjectConfig(SprucePrefab, 0.25f, 0.7f, 0.25f, 6f),
                new TerrainObjectConfig(BushPrefab, 0.5f, 0.7f, 0.7f, 0f)
            }));
        samplerConfigs.Add(RegionType.NatureReserve, new TerrainObjectSamplerConfig(SamplerType.NatureReserve, 7.5f, 7.5f, 0, 0, true,
            new List<TerrainObjectConfig>
            {
                //Not to be modified
                new TerrainObjectConfig(MaplePrefab, 1f, 0.75f, 0.4f, 0f),
                new TerrainObjectConfig(SprucePrefab, 1f, 0.7f, 0.25f, 0f),
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f),
                new TerrainObjectConfig(GrassPrefab, 1f, 0.75f, 0.4f, 0f),
            },
            new List<TerrainObjectConfig>
            {
                new TerrainObjectConfig(BushPrefab, 1f, 0.7f, 0.7f, 0f)
            }));
    }


    public static TerrainObjectSamplerConfig? GetRegionTerrainObjectSamplerConfig(RegionType type)
    {
        if(Ins.samplerConfigs.ContainsKey(type))
            return Ins.samplerConfigs[type];
        return null;
    }

	void Update () {
		
	}
}
