﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
// ReSharper disable UnusedMember.Local

#pragma warning disable 414

public class ChunkManagerWrapper : MonoBehaviour
{
    public static ChunkManagerWrapper instance;

    public Transform worldParent;

    public MacroChunkWrapper MacroChunkPrefab;
    public MesoChunkWrapper MesoChunkPrefab;

    private ChunkManager chunkManager;

    private Dictionary<Point2, MacroChunkWrapper> macroChunks;
    private Dictionary<Point2, MesoChunkWrapper> mesoChunks;

    #region for updating the world
    public Transform WorldCenter;

    private Point2 oldWorldCenterMesoChunk;

    private List<Point2> deletableMesoChunks = new List<Point2>();
    
    private bool finishedNewMesoChunks = false;

    private int frameCounter = 0;
    #endregion


    void Awake()
    {
        instance = this;
        NoiseService.Initialize();

        if (WorldCenter == null || !WorldCenter.gameObject.activeSelf)
        {
            Debug.Log("Defaulting world center to main camera");
            WorldCenter = Camera.main.transform;
        }

        macroChunks = new Dictionary<Point2, MacroChunkWrapper>();
        mesoChunks = new Dictionary<Point2, MesoChunkWrapper>();

        chunkManager = new ChunkManager();
        chunkManager.MacroChunkFinishedGeneratingAsync += OnMacroChunkFinishedGeneratingAsync;
        chunkManager.MesoChunkFinishedGeneratingAsync += OnMesoChunkFinishedGeneratingAsync;
        chunkManager.MesoChunkDeleted += OnMesoChunkDeleted;


        //Make sure there's an auto update
        oldWorldCenterMesoChunk = CoordUtil.GlobalPositionToMesoChunk(WorldCenter.position) + new Point2(1, 1);
    }

    void Update()
    {
        if (WorldCenter == null)
            return;

        frameCounter++;

        UpdateMesoChunks();
        
        if (Settings.SpawnTerrainObjects)
            UpdateTerrainObjects();
    }

    public void SetWorldCenter(Transform transform)
    {
        WorldCenter = transform;
    }

    private void UpdateTerrainObjects()
    {
        foreach (var mesoChunk in mesoChunks.Values)
        {
            foreach (var region in mesoChunk.Chunk.Data.Regions)
            {
                //Spawn any high tier chunk objects
                if (region.TerrainObjectMetaDatas != null)
                {
                    var regionDist = ChunkUtil.PointDistanceFromPolygon(WorldCenter.position, region.Outline);
                    if (!region.ObjectsPlaced && regionDist < Settings.TerrainObjectSpawnRadius * 4f)
                    {
                        //place objects
                        if (region.TerrainObjectMetaDatas == null) continue;

                        SpawnObjects(region, region.TerrainObjectMetaDatas);
                        region.SetObjectsPlaced(true);

                    }
                    else if (region.ObjectsPlaced && regionDist > Settings.TerrainObjectSpawnRadius * 4f + 25)
                    {
                        if (region.SpawnerCoroutine != null)
                            StopCoroutine(region.SpawnerCoroutine);
                        DestroyObjects(region.TerrainObjects);
                        region.SetObjectsPlaced(false);
                        region.SetTerrainObjects(null);
                    }
                }


                if (region.SubRegions == null)
                    continue;
                foreach (var subregion in region.SubRegions)
                {

                    var dist = ChunkUtil.PointDistanceFromPolygon(WorldCenter.position, subregion.Outline);
                    if (!subregion.ObjectsPlaced && dist < Settings.TerrainObjectSpawnRadius)
                    {
                        //place objects
                        if (subregion.TerrainObjectMetaDatas == null) continue;
      
                        SpawnObjects(subregion, subregion.TerrainObjectMetaDatas);
                        subregion.SetObjectsPlaced(true);

                    }
                    else if (subregion.ObjectsPlaced && dist > Settings.TerrainObjectSpawnRadius + 25)
                    {
                        if (subregion.SpawnerCoroutine != null)
                            StopCoroutine(subregion.SpawnerCoroutine);
                        DestroyObjects(subregion.TerrainObjects);
                        subregion.SetObjectsPlaced(false);
                        subregion.SetTerrainObjects(null);
                    }
                }
            }
        }
    }

    private void SpawnObjects(MesoChunkRegion subregion, List<TerrainObjectMetaData> metaDatas)
    {
        var terrainObjects = new List<GameObject>();
        subregion.SetTerrainObjects(terrainObjects);
        var spawnerCoroutine = StartCoroutine(SpawnObjectsCoroutine(terrainObjects, subregion.TerrainObjectMetaDatas));
        subregion.SetSpawnerCoroutine(spawnerCoroutine);
    }

    private IEnumerator SpawnObjectsCoroutine(List<GameObject> terrainObjects, List<TerrainObjectMetaData> metaDatas)
    {
        //Spawn max this number of objects per frame
        var batchSize = Settings.MaxObjectsInstantiatedPerFrame;

        for (int i = 0; i < metaDatas.Count; i++)
        {
            var metaData = metaDatas[i];
            var instance = Instantiate(metaData.Prefab, metaData.Position, metaData.Rotation);
            instance.transform.localScale = metaData.Scale;
            instance.transform.SetParent(transform);
            instance.isStatic = true;

            PostHandleSpecialTerrainObject(instance, metaData);

            terrainObjects.Add(instance);
            if (i % batchSize == batchSize - 1)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
        yield return null;
    }

    private void DestroyObjects(List<GameObject> objects)
    {
        StartCoroutine(DestroyObjectsCoroutine(objects));
    }

    private IEnumerator DestroyObjectsCoroutine(List<GameObject> objects)
    {
        //Delete max this number of objects per frame
        var batchSize = Settings.MaxObjectsInstantiatedPerFrame + 5;

        for (int i = 0; i < objects.Count; i++)
        {
            Destroy(objects[i]);
            if (i % batchSize == batchSize - 1)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
        yield return null;
    }

    private void PostHandleSpecialTerrainObject(GameObject instance, TerrainObjectMetaData metaData)
    {
        if (instance.name.Contains("SettlementBegin"))
        {
            var text = instance.GetComponentInChildren<Text>();
            if (text != null)
            {
                text.text = (string)metaData.Data[0];
            }
        }
        else if (instance.name.Contains("SettlementAhead") || instance.name.Contains("SettlementLeft") || instance.name.Contains("SettlementRight"))
        {
            var texts = instance.GetComponentsInChildren<Text>();
            if (texts != null && texts.Length == 2)
            {
                texts[0].text = (string)metaData.Data[0];
                texts[1].text = (string)metaData.Data[1];
            }
        } else if (instance.name.Contains("UtilityPole") && metaData.Data != null)
        {
            GenerateUtilityPoleConnections(instance, metaData.Data);
        }
    }

    private void GenerateUtilityPoleConnections(GameObject instance, object[] data)
    {
        var basePos = instance.transform.position + new Vector3(0, 7.15f, 0);
        var baseYaw = instance.transform.rotation.eulerAngles.y * Mathf.Deg2Rad;
        var baseLeft = new Vector3(Mathf.Sin(baseYaw), 0, Mathf.Cos(baseYaw));
        if (baseLeft.x > baseLeft.z)
            baseLeft *= -1;

        for (int j = 0; j < data.Length; j += 2)
        {
            var connectionPos = (Vector3)data[j] + new Vector3(0, 7.15f, 0);
            var connectionYaw = (float)data[j + 1] * Mathf.Deg2Rad;
            var connectionLeft = new Vector3(Mathf.Sin(connectionYaw), 0, Mathf.Cos(connectionYaw));
            if (connectionLeft.x > connectionLeft.z)
                connectionLeft *= -1;

            for (int i = -1; i <= 1; i += 2)
            {
                var line = new GameObject();
                line.transform.SetParent(instance.transform);
                var lineRenderer = line.AddComponent<LineRenderer>();
                lineRenderer.sharedMaterial = AssetManager.Ins.UtilityPoleLineMaterial;
                lineRenderer.widthMultiplier = 0.05f;

                var baseOffset = baseLeft * i * 0.4f;
                var connectionoffset = connectionLeft * i * 0.4f;
                
                var linePositions = GenerateUtilityPoleLinePositions(basePos + baseOffset, connectionPos + connectionoffset);
                lineRenderer.positionCount = linePositions.Length;
                lineRenderer.SetPositions(linePositions);
            }
            
        }

    }

    //Based on https://gist.github.com/Farfarer/a765cd07920d48a8713a0c1924db6d70 catenary implementation
    private Vector3[] GenerateUtilityPoleLinePositions(Vector3 start, Vector3 end)
    {
        int count = 7;

        var slack = 0.15f;

        float lineDist = Vector3.Distance(start, end);
        float lineDistH = Vector3.Distance(new Vector3(start.x, end.y, start.z), end);
        float l = lineDist + Mathf.Max(0.0001f, slack);
        float r = 0.0f;
        float s = start.y;
        float u = lineDistH;
        float v = end.y;

        float ztarget = Mathf.Sqrt(Mathf.Pow(l, 2.0f) - Mathf.Pow(v - s, 2.0f)) / (u - r);

        int loops = 30;
        int iterationCount = 0;
        int maxIterations = loops * 10; // For safety.
        bool found = false;

        float z = 0.0f;
        float ztest = 0.0f;
        float zstep = 100.0f;
        float ztesttarget = 0.0f;
        for (int i = 0; i < loops; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                iterationCount++;
                ztest = z + zstep;
                ztesttarget = (float)Math.Sinh(ztest) / ztest;

                if (float.IsInfinity(ztesttarget))
                    continue;

                if (ztesttarget == ztarget)
                {
                    found = true;
                    z = ztest;
                    break;
                }
                else if (ztesttarget > ztarget)
                {
                    break;
                }
                else
                {
                    z = ztest;
                }

                if (iterationCount > maxIterations)
                {
                    found = true;
                    break;
                }
            }

            if (found)
                break;

            zstep *= 0.1f;
        }

        float a = (u - r) / 2.0f / z;
        float p = (r + u - a * Mathf.Log((l + v - s) / (l - v + s))) / 2.0f;
        float q = (v + s - l * (float)Math.Cosh(z) / (float)Math.Sinh(z)) / 2.0f;

        var positions = new Vector3[count];
        float stepsf = count - 1;
        float stepf;
        for (int i = 0; i < count; i++)
        {
            stepf = i / stepsf;
            Vector3 pos = Vector3.zero;
            pos.x = Mathf.Lerp(start.x, end.x, stepf);
            pos.z = Mathf.Lerp(start.z, end.z, stepf);
            pos.y = a * (float)Math.Cosh(((stepf * lineDistH) - p) / a) + q;
            positions[i] = pos;
        }
        
        return positions;
    }

    private void UpdateMesoChunks()
    {
        var worldCenterMesoChunk = CoordUtil.GlobalPositionToMesoChunk(WorldCenter.position);
        if (worldCenterMesoChunk != oldWorldCenterMesoChunk)
        {
            finishedNewMesoChunks = false;
            oldWorldCenterMesoChunk = worldCenterMesoChunk;
        }
        RemoveOldChunks(worldCenterMesoChunk);
        if (!finishedNewMesoChunks)
        {
            LoadNewMesoChunks(worldCenterMesoChunk);
        }
    }

    private void LoadNewMesoChunks(Point2 centerMesoChunk)
    {
        //Start generating new mesochunks
        var r = Settings.MesoGenerationRadius;

        if (r == -1)
        {
            //Special case: no mesochunks will be loaded, load macrochunks instead
            var macroChunkPos = CoordUtil.MesoChunkToMacroChunk(centerMesoChunk);
            var macroChunkRadius = 2;
            for (int x = macroChunkPos.x - (int)(macroChunkRadius * 1.6f); x <= macroChunkPos.x + (int)(macroChunkRadius * 1.6f); x++)
            {
                for (int z = macroChunkPos.z - macroChunkRadius; z <= macroChunkPos.z + macroChunkRadius; z++)
                {
                    chunkManager.GetOrGenerateMacroChunk(new Point2(x, z));
                }
            }
        }

         //Only trigger loading a chunk every n'th frame
        if (frameCounter % Settings.MinFramesPerChunk == 0)
            return;
        
        for (int x = centerMesoChunk.x - r; x <= centerMesoChunk.x + r; x++) {
            for (int z = centerMesoChunk.z - r; z <= centerMesoChunk.z + r; z++) {
                var mesoChunkPos = new Point2(x, z);
                var generating = chunkManager.LoadMesoChunkIfNotExists(mesoChunkPos);
                if(generating)
                    //Prevent multiple chunks start loading at the same frame to prevent threading problems
                    return;

            }
        }
        finishedNewMesoChunks = true;
    }


    private void OnMesoChunkDeleted(Point2 mesoChunkPos) {
        if (mesoChunks.ContainsKey(mesoChunkPos))
        {
            var wrapper = mesoChunks[mesoChunkPos];
            mesoChunks.Remove(mesoChunkPos);
            Destroy(wrapper.gameObject);
        }
    }


    private void OnMacroChunkFinishedGeneratingAsync(MacroChunk macroChunk)
    {
        ActionQueue.Ins.Enqueue(() => OnMacroChunkLoaded(macroChunk));
    }

    private void OnMacroChunkLoaded(MacroChunk macroChunk)
    {
        var wrapper = Instantiate(MacroChunkPrefab);
        wrapper.Initialize(macroChunk);
        wrapper.transform.SetParent(worldParent, false);
        macroChunks.Add(macroChunk.Pos, wrapper);
    }

    private void OnMesoChunkFinishedGeneratingAsync(MesoChunk mesoChunk)
    {
        ActionQueue.Ins.Enqueue(() => OnMesoChunkFinishedGenerating(mesoChunk));
    }

    private void OnMesoChunkFinishedGenerating(MesoChunk mesoChunk)
    {
        var wrapper = Instantiate(MesoChunkPrefab);
        wrapper.Initialize(mesoChunk);
        mesoChunks.Add(mesoChunk.Pos, wrapper);
        wrapper.transform.SetParent(macroChunks[mesoChunk.MacroChunk.Pos].transform, false);
    }


    public void RemoveOldChunks(Point2 centerMesoChunk)
    {
        deletableMesoChunks.Clear();
        if (mesoChunks == null)
            return;
        foreach (var entry in mesoChunks) {
            var pos = entry.Key;
            if (pos.DistOnLongestAxis(centerMesoChunk) > Settings.MesoGenerationRadius + 1 && mesoChunks[pos].Chunk.MeshReady)
            {
                deletableMesoChunks.Add(pos);
            }
        }
        foreach (var chunk in deletableMesoChunks)
        {
            DeleteMesoChunk(chunk);
            chunkManager.MarkMesoChunkForDeletion(chunk);
        }
    }

    private void DeleteMesoChunk(Point2 pos)
    {
        var wrapper = mesoChunks[pos];
        Destroy(wrapper.gameObject);

        mesoChunks.Remove(pos);
    }
}
