﻿using System;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 649

public class MesoCityMeshBuilder
{

    public MeshBuilder GenerateCityMesh(MesoChunk mesoChunk)
    {
        var builder = new MeshBuilder();

        if (mesoChunk.Data.CityData != null)
        {
            var buildings = mesoChunk.Data.CityData.CityBuildings;
            if (buildings == null)
                return builder;
            foreach (var buildingMetaData in buildings)
            {
                GenerateCityBlock(builder, mesoChunk, buildingMetaData);
            }
        }

        return builder;
    }

    /// <summary>
    /// Generates a building based on the given building meta data.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="mesoChunk"></param>
    /// <param name="buildingMetaData"></param>
    private void GenerateCityBlock(MeshBuilder builder, MesoChunk mesoChunk, CityBuildingMetaData buildingMetaData)
    {
        var numTexFloors = AssetManager.Ins.NumberOfFloorsOnCityMaterials[buildingMetaData.MaterialIndex];

        var buildingOutline = ClipperUtil.ToVectors(buildingMetaData.PointOutline);

        var heightFromOrigin = CalculateBuildingHeightFromOrigin(mesoChunk, buildingOutline);

        for (int i = 0; i < buildingMetaData.PointOutline.Count; i++)
        {
            var v1 = buildingMetaData.PointOutline[i].ToVec3();
            var v2 = buildingMetaData.PointOutline[(i + 1) % buildingMetaData.PointOutline.Count].ToVec3();

            var buildingAspectRatio = buildingMetaData.Height / (v2 - v1).magnitude;

            var numberOfFloors = Mathf.Round(buildingMetaData.Height / 2.5f);


            var heightTexMultiplier = numberOfFloors / (float)numTexFloors;

//            var xDist = (v2 - v1).magnitude;
            var widthTexMultiplier = heightTexMultiplier / buildingAspectRatio;

            var widthRemainder = widthTexMultiplier % 1;
            if (widthRemainder < 0.2f || widthRemainder > 0.8f)
            {
                widthTexMultiplier = Mathf.Round(widthTexMultiplier);
            }
            

            GenerateWall(builder, v1, v2, widthTexMultiplier, heightTexMultiplier, heightFromOrigin, buildingMetaData.Height, buildingMetaData.MaterialIndex);
        }
        
        //The roof
        if (ChunkUtil.IsConvex(buildingOutline))
        {
            GenerateFan(builder, buildingOutline, Vector3.zero, Vector3.right, 4f, heightFromOrigin,
                buildingMetaData.Height, buildingMetaData.MaterialIndex);
        }
        else
        {
            GenerateNonConvexRoof(builder, buildingOutline, Vector3.zero, Vector3.right, 4f, heightFromOrigin,
                buildingMetaData.Height, buildingMetaData.MaterialIndex);
        }
    }

    /// <summary>
    /// Generates a nonconvex polygon roof for the given building outline.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="buildingOutline"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="heightFromOrigin"></param>
    /// <param name="height"></param>
    /// <param name="materialIndex"></param>
    private void GenerateNonConvexRoof(MeshBuilder builder, List<Vector3> buildingOutline, Vector3 leftEdge,
        Vector3 rightEdge, float texCoordMultiplier, float heightFromOrigin, float height, int materialIndex)
    {
        var roofVertices = buildingOutline.ToArray();
        var triangulator = new Triangulator(roofVertices);

        var roofIndices = triangulator.Triangulate();

        builder.AddIndices(materialIndex, roofIndices);

        for (int i = 0; i < roofVertices.Length; i++)
        {
            var texCoord = GetTextureCoords(leftEdge, rightEdge, roofVertices[i], texCoordMultiplier);
            builder.AddTexCoord(texCoord);

            roofVertices[i].y = height + heightFromOrigin;
        }

        builder.AddVertices(roofVertices);
    }

    /// <summary>
    /// Calculates the min height of the buidling based on the terrain height.
    /// </summary>
    /// <param name="mesoChunk"></param>
    /// <param name="buildingOutline"></param>
    /// <returns></returns>
    private float CalculateBuildingHeightFromOrigin(MesoChunk mesoChunk, List<Vector3> buildingOutline)
    {
        var min = (float)Const.HEIGHTMAP_RANGE_IN_METERS;

        for (int i = 0; i < buildingOutline.Count; i++)
        {
            var height = mesoChunk.GetHeightAt(buildingOutline[i]);
            if (height < min)
                min = height;
        }

        return min - 0.2f;
    }

    /// <summary>
    /// Generates a wall with the given height and position (left, right corners defined).
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <param name="widthTexMultiplier"></param>
    /// <param name="heightTexMultiplier"></param>
    /// <param name="heightFromOrigin"></param>
    /// <param name="height"></param>
    /// <param name="materialIndex"></param>
    private void GenerateWall(MeshBuilder builder, Vector3 left, Vector3 right, float widthTexMultiplier, float heightTexMultiplier, float heightFromOrigin, float height, int materialIndex)
    {
        builder.AddQuadIndices(materialIndex);

        var texCoords = new Vector2[4];
        texCoords[0] = new Vector2(0, 0);
        texCoords[1] = new Vector2(widthTexMultiplier, 0);
        texCoords[2] = new Vector2(0, heightTexMultiplier);
        texCoords[3] = new Vector2(widthTexMultiplier, heightTexMultiplier);
        builder.AddTexCoords(texCoords);

        var verts = new Vector3[4];
        verts[0] = left + new Vector3(0, heightFromOrigin, 0);
        verts[1] = right + new Vector3(0, heightFromOrigin, 0);
        verts[2] = left + new Vector3(0, height + heightFromOrigin, 0);
        verts[3] = right + new Vector3(0, height + heightFromOrigin, 0);
        builder.AddVertices(verts);
    }

    /// <summary>
    /// Generates a fan mesh for ordered vertices.
    /// </summary>
    /// <param name="builder"></param>
    /// <param name="orderedVertices"></param>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <param name="heightFromOrigin"></param>
    /// <param name="height"></param>
    /// <param name="materialIndex"></param>
    private void GenerateFan(MeshBuilder builder, List<Vector3> orderedVertices, Vector3 leftEdge, Vector3 rightEdge, float texCoordMultiplier, float heightFromOrigin, float height, int materialIndex)
    {
        if (orderedVertices.Count < 3)
        {
            return;
        }

        var vertices = new Vector3[3];
        vertices[0] = orderedVertices[0] + new Vector3(0, height + heightFromOrigin, 0);
        var texCoords = new Vector2[3];
        texCoords[0] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[0], texCoordMultiplier);

        for (int i = 1; i < orderedVertices.Count - 1; i++)
        {
            vertices[1] = orderedVertices[i + 1] + new Vector3(0, height + heightFromOrigin, 0);
            vertices[2] = orderedVertices[i] + new Vector3(0, height + heightFromOrigin, 0);
            builder.AddTriangleIndices(materialIndex);
            builder.AddVertices(vertices);

            texCoords[1] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i + 1], texCoordMultiplier);
            texCoords[2] = GetTextureCoords(leftEdge, rightEdge, orderedVertices[i], texCoordMultiplier);
            builder.AddTexCoords(texCoords);
        }
    }

    /// <summary>
    /// Gets the texturecoords at given point when texture coordinates at 'leftEdge' and 'rightEdge' are defined to be (0,0) and (1,0).
    /// </summary>
    /// <param name="leftEdge"></param>
    /// <param name="rightEdge"></param>
    /// <param name="point"></param>
    /// <param name="texCoordMultiplier"></param>
    /// <returns></returns>
    private Vector2 GetTextureCoords(Vector3 leftEdge, Vector3 rightEdge, Vector3 point, float texCoordMultiplier)
    {
        var right = (rightEdge - leftEdge).normalized;
        var fwd = new Vector3(-right.z, 0, right.x);


        var middle = (leftEdge + rightEdge) / 2;

        var maxDist = (rightEdge - middle).magnitude;

        var xProjection = ChunkUtil.GetPointProjectionOnLine(point, middle, middle + fwd);
        var yProjection = ChunkUtil.GetPointProjectionOnLine(point, leftEdge, rightEdge);

        var distX = (xProjection - point).magnitude;
        if (ChunkUtil.PointToTheRightOfLine(point, middle, middle + fwd))
        {
            distX *= -1f;
        }

        var distY = (yProjection - point).magnitude;

       return new Vector2(distX / maxDist * texCoordMultiplier, distY / maxDist * texCoordMultiplier);
    }

}
