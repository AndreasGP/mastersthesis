﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;
using Random = System.Random;

public class MesoChunkCityGenerator
{

    private MesoChunk mesoChunk;

    private Point2 mesoChunkPos;

    private MacroChunk macroChunk;

    private MesoRoadData mesoRoadData;

    private MesoChunkCityData mesoCityData;
    
    public MesoChunkCityGenerator(MesoChunk mesoChunk, MesoRoadData mesoRoadData, MesoChunkCityData mesoCityData)
    {
        this.mesoChunk = mesoChunk;
        this.mesoRoadData = mesoRoadData;
        this.macroChunk = mesoChunk.MacroChunk;
        this.mesoCityData = mesoCityData;
        mesoChunkPos = mesoChunk.Pos;
    }
    
    /// <summary>
    /// Generates all the city street and block mesochunkregions if the chunk contains a city.
    /// </summary>
    /// <returns></returns>
    public List<MesoChunkRegion> GenerateCityStreetsAndBlocksAsync()
    {
        var cityRegions = new List<MesoChunkRegion>();

        var random = ChunkUtil.GetMesoChunkRandom(mesoChunkPos);

        var blockClipper = new Clipper();
        var shoulderClipper = new Clipper();

        if (mesoCityData.CityOnly)
        {
            //Just clip chunk bounds
            blockClipper.AddPath(mesoChunk.ChunkRectOutline, PolyType.ptSubject, true);
            shoulderClipper.AddPath(mesoChunk.ChunkRectOutline, PolyType.ptSubject, true);
        }
        else
        {
            //Only clip the area of the chunk actually covered by a city
            blockClipper.AddPaths(mesoCityData.CityPolys, PolyType.ptSubject, true);
            shoulderClipper.AddPaths(mesoCityData.CityPolys, PolyType.ptSubject, true);
        }

        var roadPolys = mesoRoadData.RoadPolys;
        var shoulderPolys = mesoRoadData.ShoulderPolys;

        var roadPolysWithCityStreets = new List<List<IntPoint>>();
        roadPolysWithCityStreets.AddRange(roadPolys);
        //Determine the initial street segments placed on a grid, ignoring exact intersections with highways and side roads.
        var initialStreetSegments = GenerateInitialStreetSegments(random, blockClipper, roadPolysWithCityStreets);
        mesoRoadData.SetRoadAndStreetPolys(roadPolysWithCityStreets);

        //Next, generate the final street segments that have been culled by highways and side roads to prevent overlaps.
        GenerateFinalStreetSegments(initialStreetSegments, roadPolys);


        foreach (var cityPoly in mesoCityData.CityPolys)
        {
            var region = new MesoChunkRegion(RegionType.City, cityPoly, ClipperUtil.ToVectors(cityPoly), true);
            cityRegions.Add(region);
        }

        shoulderClipper.AddPaths(shoulderPolys, PolyType.ptClip, true);
        var intersectingShoulderPolys = new List<List<IntPoint>>();
        shoulderClipper.Execute(ClipType.ctIntersection, intersectingShoulderPolys, PolyFillType.pftNonZero,
            PolyFillType.pftNonZero);

        var subregions = new List<MesoChunkRegion>();

        foreach (var intersectingShoulderPoly in intersectingShoulderPolys)
        {
            var subregion = new MesoChunkRegion(RegionType.Sidewalk, intersectingShoulderPoly, ClipperUtil.ToVectors(intersectingShoulderPoly), true);
            subregions.Add(subregion);
        }
        
        //Determine the polys of land between roads and then offset some of them inwards so they are not too close to the roads
        blockClipper.AddPaths(shoulderPolys, PolyType.ptClip, true);

        var initialPlotPolys = new List<List<IntPoint>>();
        blockClipper.Execute(ClipType.ctDifference, initialPlotPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        ProcessInitialPlotPolys(random, initialPlotPolys, subregions);

        cityRegions[0].SetSubRegions(subregions);

        return cityRegions;
    }


    /// <summary>
    /// Generates the initial street segments that overlap with side roads and highways. 
    /// </summary>
    /// <param name="random">Chunk-based random for determining some values.</param>
    /// <param name="blockClipper">A clipper that will be populated with the polygons of the initial street segments to be used later for determining land plots between roads and streets.</param>
    /// <returns></returns>
    private List<StreetSegment> GenerateInitialStreetSegments(System.Random random, Clipper blockClipper, List<List<IntPoint>> roadPolysWithStreets)
    {
        var w = 128;//random.NextDouble() < 0.05 ? Const.CITY_BLOCK_WIDTH_IN_METERS * 2 : Const.CITY_BLOCK_WIDTH_IN_METERS;

        var r = random.NextDouble();

        if (r < 0.5)
            w = 172;
        else if (r < 0.66)
            w = 86;

        var streetW2 = Const.STREET_WIDTH_IN_METERS / 2;

        var initialStreetSegments = new List<StreetSegment>();

        var right = new Vector3(streetW2, 0, 0);
        var up = new Vector3(0, 0, streetW2);

        var startX = mesoChunk.Rect.xMin + streetW2;
        var startZ = mesoChunk.Rect.yMin + streetW2;

        var steps = (int)Mathf.Ceil(Const.MESO_CHUNK_WIDTH_IN_METERS / w);

        for (int x = 0; x < steps; x++)
        {
            for (int z = 0; z < steps; z++)
            {
                var start = new Vector3(startX + x * w, 0, startZ + z * w);

                var startInsideCity = ChunkUtil.InsideSimplePolygon(start, mesoCityData.City.OutterOutline);

                //Upwards path
                if (random.NextDouble() < 0.9f)
                {
                    var endUp = ClipInsideChunk(new Vector3(startX + x * w, 0, startZ + (z + 1)*w), mesoChunk.Rect, Const.STREET_WIDTH_IN_METERS / 2 + 0.01f);

                    var endUpInsideCity = ChunkUtil.InsideSimplePolygon(endUp, mesoCityData.City.OutterOutline);
                    var midInsideCity = ChunkUtil.InsideSimplePolygon(start + (endUp - start) * 0.5f, mesoCityData.City.OutterOutline);

                    if (IsValidPositionForCityStreet(start, Vector3.forward) &&
                        IsValidPositionForCityStreet(endUp, Vector3.forward) &&
                        midInsideCity && (startInsideCity || endUpInsideCity))
                    {
                        var streetOutline = new List<IntPoint>
                        {
                            new IntPoint(start - right - up),
                            new IntPoint(start + right - up),
                            new IntPoint(endUp + right - up),
                            new IntPoint(endUp - right - up)
                        };
                        blockClipper.AddPath(streetOutline, PolyType.ptClip, true);
                        var streetSegment = new StreetSegment(streetOutline, ClipperUtil.ToVectors(streetOutline),
                            start - right - up, start + right - up, false);
                        initialStreetSegments.Add(streetSegment);
                        roadPolysWithStreets.Add(streetOutline);
                    }
                }

                //Rightwards path
                if (random.NextDouble() < 0.9f)
                {
                    var endRight = ClipInsideChunk(new Vector3(startX + (x + 1) * w, 0, startZ + z * w), mesoChunk.Rect, Const.STREET_WIDTH_IN_METERS / 2 + 0.01f);

                    var endRightInsideCity = ChunkUtil.InsideSimplePolygon(endRight, mesoCityData.City.OutterOutline);
                    var midInsideCity = ChunkUtil.InsideSimplePolygon(start + (endRight - start) * 0.5f, mesoCityData.City.OutterOutline);

                    if (IsValidPositionForCityStreet(start, Vector3.right) &&
                        IsValidPositionForCityStreet(endRight, Vector3.right) &&
                        midInsideCity && (startInsideCity || endRightInsideCity))
                    {
                        var streetOutline = new List<IntPoint>
                        {
                            new IntPoint(start + up - right),
                            new IntPoint(start - up - right),
                            new IntPoint(endRight - up - right),
                            new IntPoint(endRight + up - right)
                        };
                        blockClipper.AddPath(streetOutline, PolyType.ptClip, true);

                        var streetSegment = new StreetSegment(streetOutline, ClipperUtil.ToVectors(streetOutline),
                            start + up - right, start - up - right, false);
                        initialStreetSegments.Add(streetSegment);
                        roadPolysWithStreets.Add(streetOutline);
                    }
                }
            }
        }

        return initialStreetSegments;
    }

    /// <summary>
    /// Constraints the given vector to be inside the chunkRect with a minimal distance 'e' from the border.
    /// </summary>
    /// <param name="vector"></param>
    /// <param name="chunkRect"></param>
    /// <returns></returns>
    private Vector3 ClipInsideChunk(Vector3 vector, Rect chunkRect, float e)
    {
        if (vector.x < chunkRect.xMin - e)
            vector.x = chunkRect.xMin - e;
        if (vector.x > chunkRect.xMax + e)
            vector.x = chunkRect.xMax + e;
        if (vector.z < chunkRect.yMin - e)
            vector.z = chunkRect.yMin - e;
        if (vector.z > chunkRect.yMax + e)
            vector.z = chunkRect.yMax + e;
        return vector;
    }

    /// <summary>
    /// Generates the final street segments that have been culled by highways and side roads and stores them in mesoroaddata.
    /// </summary>
    /// <param name="initialStreetSegments"></param>
    /// <param name="roadPolys"></param>
    private void GenerateFinalStreetSegments(List<StreetSegment> initialStreetSegments, List<List<IntPoint>> roadPolys)
    {
        var streetSegments = new List<StreetSegment>();
        var clipper = new Clipper();

        var crosswalks = mesoRoadData.Crosswalks ?? new List<Crosswalk>();
        var crosswalkClipper = new Clipper();

        foreach (var initialStreetSegment in initialStreetSegments)
        {
            clipper.Clear();
            clipper.AddPaths(roadPolys, PolyType.ptClip, true);
            clipper.AddPath(initialStreetSegment.PointOutline, PolyType.ptSubject, true);

            var streetPolys = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctDifference, streetPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

            foreach (var streetPoly in streetPolys)
            {

                var left = initialStreetSegment.Left;
                var right = initialStreetSegment.Right;

                var streetSegment = new StreetSegment(streetPoly, ClipperUtil.ToVectors(streetPoly), left, right, false);
                streetSegments.Add(streetSegment);


                #region generate crosswalks
                var mid = (left + right) / 2;
                var rightDir = (right - left).normalized;
                var fwd = new Vector3(-rightDir.z, 0, rightDir.x);

                for (int i = 0; i < 2; i++)
                {
                    var crosswalkInitialPoly = new List<IntPoint>();
                    var crossWalkStart = 8 + i * (Const.CITY_BLOCK_WIDTH_IN_METERS - 14);
                    var crossWalkEnd = 11 + i * (Const.CITY_BLOCK_WIDTH_IN_METERS - 14);

                    crosswalkInitialPoly.Add(
                        new IntPoint(mid + fwd * crossWalkStart + rightDir * Const.STREET_WIDTH_IN_METERS / 2));
                    crosswalkInitialPoly.Add(
                        new IntPoint(mid + fwd * crossWalkStart - rightDir * Const.STREET_WIDTH_IN_METERS / 2));
                    crosswalkInitialPoly.Add(
                        new IntPoint(mid + fwd * crossWalkEnd - rightDir * Const.STREET_WIDTH_IN_METERS / 2));
                    crosswalkInitialPoly.Add(
                        new IntPoint(mid + fwd * crossWalkEnd + rightDir * Const.STREET_WIDTH_IN_METERS / 2));

                    MesoChunkRoadGenerator.GenerateCrosswalk(crosswalkClipper, crosswalkInitialPoly, streetPoly,
                        mid + fwd * crossWalkStart - rightDir * Const.STREET_WIDTH_IN_METERS / 2,
                        mid + fwd * crossWalkEnd - rightDir * Const.STREET_WIDTH_IN_METERS / 2, crosswalks);

                }

                #endregion
            }


            mesoRoadData.SetCrosswalks(crosswalks);


            roadPolys.Add(initialStreetSegment.PointOutline);
        }
        mesoRoadData.SetStreetSegments(streetSegments);
    }

    /// <summary>
    /// Processes the initial plot polys and sends them for further subdivision between residential, industrial and commercial generation algorithms.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="initialPlotPolys"></param>
    /// <param name="subregions"></param>
    private void ProcessInitialPlotPolys(System.Random random, List<List<IntPoint>> initialPlotPolys, List<MesoChunkRegion> subregions)
    {
        mesoCityData.SetCityBlocks(initialPlotPolys);
        
        var populator = new MesoChunkTerrainPopulator();

        var offsetter = new ClipperOffset();
        for (int i = initialPlotPolys.Count - 1; i >= 0; i--)
        {
            var initialPlotPoly = initialPlotPolys[i];

            var area = ChunkUtil.PolygonArea(initialPlotPoly);
            if (area < 2500)
            {
                //Fill with plants if the region is tiny
                CreatePark(random, initialPlotPoly, subregions, populator);
                continue;
            }

            var centroid = ChunkUtil.CentroidOfPolygon(initialPlotPoly).ToVec3();
            CityAreaType type;

            if (!ChunkUtil.InsideSimplePolygon(centroid, mesoCityData.City.InnerOutline))
            {
                //Always force city edges to be suburban
                type = CityAreaType.LowResidential;
            }
            else
            {
                type = mesoCityData.City.GetClosestCityCenterType(centroid);
            }
                

            if (type == CityAreaType.LowResidential)
            {
                GenerateSuburbanBuildingPlots(random, initialPlotPoly, populator, subregions, offsetter);
            } else
            {
                GenerateProceduralBuildingPlot(random, type, initialPlotPoly, offsetter, subregions);
            }

        }
    }

    /// <summary>
    /// Generates a park within the given plotpoly and generates a corresponding mesochunkregion for it.
    /// Populator argument optional (if null provided, instantiates a new one).
    /// </summary>
    /// <param name="random"></param>
    /// <param name="plotPoly"></param>
    /// <param name="subregions"></param>
    /// <param name="populator"></param>
    private void CreatePark(System.Random random, List<IntPoint> plotPoly, List<MesoChunkRegion> subregions, MesoChunkTerrainPopulator populator)
    {
        populator = populator ?? new MesoChunkTerrainPopulator();

        var outline = ClipperUtil.ToVectors(plotPoly);

        var r = random.NextDouble();
        var type = RegionType.SubResidenceWithTreesAndBushes;
        if (r < 0.25)
            type = RegionType.SubResidenceWithTrees;
        if (r < 0.4)
            type = RegionType.SubResidenceWithSparsePlants;

        var subregion = new MesoChunkRegion(type, plotPoly, outline, true);
        populator.GenerateSubregionTerrainObjectsAsync(mesoChunk, subregion, 2);
        subregions.Add(subregion);
    }

    private void GenerateProceduralBuildingPlot(System.Random random, CityAreaType type, List<IntPoint> initialPlotPoly, ClipperOffset offsetter, List<MesoChunkRegion> subregions)
    {
        var outline = ClipperUtil.ToVectors(initialPlotPoly);
        offsetter.Clear();
        offsetter.AddPath(initialPlotPoly, JoinType.jtMiter, EndType.etClosedPolygon);

        var offsetPolys = new List<List<IntPoint>>();

        var offsetDist = 2 + random.Next(0, 7);

        offsetter.Execute(ref offsetPolys, -offsetDist * ClipperUtil.COORD_MULTIPLIER);
        
        if (offsetPolys.Count == 1)
        {
            var offsetPoly = offsetPolys[0];

            var centroid = ChunkUtil.CentroidOfPolygon(offsetPoly);

            //Small chance to not generate a building and make a park instead
            var generateBuilding = random.NextDouble() < 0.98f;

            if (generateBuilding && !ChunkUtil.InsideSimplePolygon(centroid.ToVec3(), mesoCityData.City.InnerOutline))
            {
                //This area is near the edge of the city
                generateBuilding = random.NextDouble() < 0.6f;
            }

            if (generateBuilding)
            {
                var subregion = new MesoChunkRegion(RegionType.Sidewalk, initialPlotPoly, outline, true);
                subregions.Add(subregion);

                if (type != CityAreaType.Industral && ChunkUtil.IsConvex(offsetPoly))
                {
                    var buildingPlots = SubdivideIntoBuildingPlots(offsetPoly, 20 + (float)random.NextDouble() * 20, 100);
                    GenerateBuildingsForBuildingPlot(random, type, buildingPlots);
                }
                else
                {
                    //Don't subdivide

                    if (type == CityAreaType.Industral)
                    {
                        GenerateIndustrialBuildingsForBuildingPlot(random, offsetPolys, offsetter, subregion);
                    }
                    else
                    {
                        GenerateBuildingsForBuildingPlot(random, type, offsetPolys);
                    }

                }
            }
            else
            {
                CreatePark(random, initialPlotPoly, subregions, null);
            }
        }
        else
        {
            CreatePark(random, initialPlotPoly, subregions, null);
        }
    }


    /// <summary>
    /// Generates suburban buildings for the given plot (initialPlotPoly) and generates a mesochunkregion for it.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="initialPlotPoly"></param>
    /// <param name="populator"></param>
    /// <param name="subregions"></param>
    /// <param name="offsetter"></param>
    private void GenerateSuburbanBuildingPlots(System.Random random, List<IntPoint> initialPlotPoly, MesoChunkTerrainPopulator populator, List<MesoChunkRegion> subregions, ClipperOffset offsetter)
    {

        var initialOutline = ClipperUtil.ToVectors(initialPlotPoly);
        //First generate one objectless subregion which will be used to generate the terrain for this initialPlotPoly

        var terrainMeshRegion = new MesoChunkRegion(RegionType.Plain, initialPlotPoly, initialOutline, true);
        subregions.Add(terrainMeshRegion);

        //Offset the initial plot a bit inwards
        offsetter.Clear();
        offsetter.AddPath(initialPlotPoly, JoinType.jtMiter, EndType.etClosedPolygon);
        var offsetPolys = new List<List<IntPoint>>();
        var offsetDist = 1 + random.Next(0, 2);
        offsetter.Execute(ref offsetPolys, -offsetDist * ClipperUtil.COORD_MULTIPLIER);
        foreach (var offsetPoly in offsetPolys)
        { //Subdivide into residential plots
            var gardenPlots = new List<List<IntPoint>>();
            SubdivideIntoGardenPlots(new Clipper(), offsetPoly, gardenPlots, 3);

            var offsetOutline = ClipperUtil.ToVectors(offsetPoly);

            foreach (var gardenPlot in gardenPlots)
            {
                var gardenOutline = ClipperUtil.ToVectors(gardenPlot);

                var terrainObjects = new List<TerrainObjectMetaData>();
                List<IntPoint> housePointOutline;
                GenerateGardenAndHouse(random, offsetOutline, gardenOutline, terrainObjects, out housePointOutline);

                var type = RegionType.SubResidenceWithTrees;
                var typeRandom = random.NextDouble();
                if (typeRandom < 0.25)
                    type = RegionType.SubResidenceWithBushes;
                else if (typeRandom < 0.5)
                    type = RegionType.SubResidenceWithTreesAndBushes;
                else if (typeRandom < 0.75)
                    type = RegionType.SubResidenceWithSparsePlants;
                else if (typeRandom < 0.95)
                    type = RegionType.Plain;

                if (housePointOutline == null)
                {
                    //Default to trees and bushes if no house placed
                    type = RegionType.SubResidenceWithTreesAndBushes;
                }


                var subregion = new MesoChunkRegion(type, gardenPlot, gardenOutline, true);
                subregion.SetTerrainObjectMetaDatas(terrainObjects);
                //Since the terrain for this plot has been already handled, disable terrain gen for this subregion
                subregion.SetGenerateTerrainMesh(false);
                subregions.Add(subregion);
                if (housePointOutline != null)
                {
                    var ignorableOutlines = new List<List<Vector3>> { ClipperUtil.ToVectors(housePointOutline) };
                    subregion.SetIgnorableOutlines(ignorableOutlines);
                }

                populator.GenerateSubregionTerrainObjectsAsync(mesoChunk, subregion, 3);
            }
        }
    }

    /// <summary>
    /// Generates a garden and a house inside the 'gardenOutline' which is inside a plot defined by 'plotOutline'.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="plotOutline"></param>
    /// <param name="gardenOutline"></param>
    /// <param name="terrainObjects"></param>
    /// <param name="housePointOutline">the generated house point outline</param>
    private void GenerateGardenAndHouse(System.Random random, List<Vector3> plotOutline, List<Vector3> gardenOutline, List<TerrainObjectMetaData> terrainObjects, out List<IntPoint> housePointOutline)
    {
        housePointOutline = null;
        for (int i = 0; i < gardenOutline.Count; i++)
        {
            var v1 = gardenOutline[i];
            var v2 = gardenOutline[(i + 1) % gardenOutline.Count];

            var housePlaced = false;

            for (int j = 0; j < plotOutline.Count; j++)
            {
                var p1 = plotOutline[j];
                var p2 = plotOutline[(j + 1) % plotOutline.Count];

                if (ChunkUtil.PointDistanceFromLineSegment(v1, p1, p2) < 0.2f &&
                    ChunkUtil.PointDistanceFromLineSegment(v2, p1, p2) < 0.2f)
                {
                    //Garden edge v1-v2 is next to a road

                    if ((v1 - v2).magnitude > 20)
                    {
                        //Edge is at least 20m long, consider it possibly suitable for house placement

                        var dir = (v1 - v2).normalized;
                        var right = new Vector3(dir.z, 0, -dir.x);

                        var housePos = v1 + (v2 - v1) * 0.5f + right * 8.5f;
                        housePos.y = mesoChunk.GetHeightAt(housePos);
                        var houseRotationRad = Mathf.Atan2(right.z, right.x);

                        var gatePos = v1 + (v2 - v1) * 0.5f + right;

                        var width = 12;
                        var height = 12;
                        var scale = 1f;

                        var placingChurch = false;
                        if (random.NextDouble() < 0.01)
                        {
                            height = 18;
                            placingChurch = true;
                            scale = 2.5f;
                        }

                        if (ChunkUtil.IsValidHousePosition(housePos, houseRotationRad, gardenOutline, width, height, out housePointOutline))
                        {
                            var housePrefabs = AssetManager.Ins.HousePrefabs;
                            var modelIndex = random.Next() % housePrefabs.Count;
                            var prefab = housePrefabs[modelIndex];

                            if (placingChurch)
                                prefab = AssetManager.Ins.ChurchPrefab;

                            var obj = TerrainObjectMetaData.Generate(prefab, housePos, 180 - houseRotationRad * Mathf.Rad2Deg, scale, 0, 0);
                            terrainObjects.Add(obj);

                            if (random.NextDouble() < 0.9)
                            {
                                ChunkUtil.GenerateFenceTerrainObjects(mesoChunk, gardenOutline, terrainObjects,
                                    gatePos, 2);
                            }

                            housePlaced = true;
                            break;
                        }
                    }
                }
            }

            if (housePlaced)
                break;
        }


    }

    /// <summary>
    /// Subdivides the provided 'points' list into multiple garden plots (returned as outPolygons). Subdivision is repeated until 'depth' is 0. 
    /// </summary>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="outPolygons"></param>
    /// <param name="depth"></param>
    private void SubdivideIntoGardenPlots(Clipper clipper, List<IntPoint> points, List<List<IntPoint>> outPolygons, int depth)
    {

        var vertices = ClipperUtil.ToVectors(points);
        if (depth == 0 || ChunkUtil.PolygonArea(points) < 195)
        {
            outPolygons.Add(points);
            return;
        }

        var longestEdgeI = ChunkUtil.LongestEdge(points);
        var centroid = ChunkUtil.CentroidOfPolygon(vertices);

        var start = vertices[longestEdgeI];
        var end = vertices[(longestEdgeI + 1) % vertices.Count];

        var clipPoint1 = ChunkUtil.GetPointProjectionOnLine(centroid, start, end);

        var clipPoint2 = centroid;

        CutGardenPlots(clipper, points, clipPoint1, clipPoint2, outPolygons, depth);
    }

    /// <summary>
    /// Cuts the given polygon 'points' with a line through start and end and continues until recursion depth 0 is reached.
    /// </summary>
    /// <param name="clipper"></param>
    /// <param name="points"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="outPolygons"></param>
    /// <param name="depth"></param>
    private void CutGardenPlots(Clipper clipper, List<IntPoint> points, Vector3 start, Vector3 end, List<List<IntPoint>> outPolygons, int depth)
    {
        var dir = (start - end).normalized;
        var right = new Vector3(-dir.z, 0, dir.x);

        var w = 2000f;
        var s1 = start + dir * w;
        var s2 = end - dir * w;
        var e1 = s1 + right * w * 1.42f;
        var e2 = s2 + right * w * 1.42f;

        var clip = new List<IntPoint>();
        clip.Add(new IntPoint(s1));
        clip.Add(new IntPoint(s2));
        clip.Add(new IntPoint(e2));
        clip.Add(new IntPoint(e1));

        clipper.Clear();
        clipper.AddPath(points, PolyType.ptSubject, true);
        clipper.AddPath(clip, PolyType.ptClip, true);

        var poly1s = new List<List<IntPoint>>();
        var poly2s = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, poly1s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        clipper.Execute(ClipType.ctDifference, poly2s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        if (poly1s.Count > 0)
        {
            foreach (var poly in poly1s)
            {
                SubdivideIntoGardenPlots(clipper, poly, outPolygons, depth - 1);
            }
        }

        if (poly2s.Count > 0)
        {
            foreach (var poly in poly2s)
            {
                SubdivideIntoGardenPlots(clipper, poly, outPolygons, depth - 1);
            }
        }

    }

    /// <summary>
    /// Generates industrial buildings and gardens for the given plots.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="buildingPlots"></param>
    /// <param name="offsetter"></param>
    /// <param name="region"></param>
    private void GenerateIndustrialBuildingsForBuildingPlot(System.Random random, List<List<IntPoint>> buildingPlots, ClipperOffset offsetter, MesoChunkRegion region)
    {
        var terrainObjects = region.TerrainObjectMetaDatas ?? new List<TerrainObjectMetaData>();
        var cityBuildings = mesoCityData.CityBuildings ?? new List<CityBuildingMetaData>();

        if (random.NextDouble() < 0.67)
        {
            foreach (var buildingPlot in buildingPlots)
            {   
                //Generate a fenced building
                var outline = ClipperUtil.ToVectors(buildingPlot);
                offsetter.Clear();
                offsetter.AddPath(buildingPlot, JoinType.jtMiter, EndType.etClosedPolygon);

                var offsetPolys = new List<List<IntPoint>>();

                var offsetDist = 15 + random.Next(0, 10);

                var area = ChunkUtil.PolygonArea(buildingPlot);

                if (area > 23000)
                {
                    offsetDist += 8;
                }

                if (area < 15000)
                {
                    offsetDist -= 10;
                }

                if (area < 6000)
                {
                    offsetDist -= 10;
                    offsetDist = Mathf.Clamp(offsetDist, 1, 100);
                }


                offsetter.Execute(ref offsetPolys, -offsetDist * ClipperUtil.COORD_MULTIPLIER);

                if (offsetPolys.Count == 1)
                {
                    var offsetPoly = offsetPolys[0];

                    var centroid = ChunkUtil.CentroidOfPolygon(offsetPoly).ToVec3();

                    var r = random.NextDouble();

                    var materialIndex =  r < 0.33 ? 4 : (r < 0.67 ? 5 : 6);

                    var height = 10f + (float)random.NextDouble() * 10;

                    if (!ChunkUtil.InsideSimplePolygon(centroid, mesoCityData.City.InnerOutline))
                    {
                        //This area is near the edge of the city so lets reduce the height drastically
                        height *= 0.5f;
                    }

                    cityBuildings.Add(new CityBuildingMetaData(offsetPoly, height, materialIndex));
                }

                ChunkUtil.GenerateFenceTerrainObjects(mesoChunk, outline, terrainObjects,
                    Vector3.zero, 2, true);

            }
        }
        else
        {

            foreach (var buildingPlot in buildingPlots)
            {
                var centroid = ChunkUtil.CentroidOfPolygon(buildingPlot).ToVec3();

                var materialIndex = random.NextDouble() < 0.5 ? 4 : 5;

                var height = 10f + (float) random.NextDouble() * 10;

                if (!ChunkUtil.InsideSimplePolygon(centroid, mesoCityData.City.InnerOutline))
                {
                    //This area is near the edge of the city so lets reduce the height drastically
                    height *= 0.5f;
                }

                cityBuildings.Add(new CityBuildingMetaData(buildingPlot, height, materialIndex));
            }

        }

        mesoCityData.SetCityBuildings(cityBuildings);
        region.SetTerrainObjectMetaDatas(terrainObjects);
    }


    /// <summary>
    /// Generates commercial and tall commercial buildings for the given plots.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="type"></param>
    /// <param name="buildingPlots"></param>
    private void GenerateBuildingsForBuildingPlot(System.Random random, CityAreaType type, List<List<IntPoint>> buildingPlots)
    {

        var cityBuildings = mesoCityData.CityBuildings ?? new List<CityBuildingMetaData>();
        foreach (var buildingPlot in buildingPlots)
        {
            var maxIndex = AssetManager.Ins.maxCityMaterialIndex;
            var hash = ChunkUtil.Hash((int)buildingPlot[0].X, (int)buildingPlot[1].Y);
            var centroid = ChunkUtil.CentroidOfPolygon(buildingPlot).ToVec3();

            var materialIndex = random.Next(0, 2) + (type == CityAreaType.Commercial ? 2 : 0);
            
            var height = 10f + (float)random.NextDouble() * 20;

            if (random.NextDouble() < 0.1)
            {
                height += 15f;
            }

            if (!ChunkUtil.InsideSimplePolygon(centroid, mesoCityData.City.InnerOutline))
            {
                //This area is near the edge of the city so lets reduce the height drastically
                height *= 0.5f;
            }

            cityBuildings.Add(new CityBuildingMetaData(buildingPlot, height, materialIndex));
        }
        mesoCityData.SetCityBuildings(cityBuildings);
    }

    /// <summary>
    /// Subdivides the given outline into multiple building plots.
    /// </summary>
    /// <param name="poly"></param>
    /// <param name="buildingWidth"></param>
    /// <param name="buildingMinArea"></param>
    /// <returns></returns>
    private List<List<IntPoint>> SubdivideIntoBuildingPlots(List<IntPoint> poly, float buildingWidth, float buildingMinArea)
    {
        var w = Const.MESO_CHUNK_WIDTH_IN_METERS ;
        var vertices = ClipperUtil.ToVectors(poly);
        var centroid = ChunkUtil.CentroidOfPolygon(vertices);

        var buildingPlots = new List<List<IntPoint>>();
        
        var remainingPoly = poly;//isConvex ? poly : HullUtil.ConvexHull(poly);

        var edges = new List<Pair<Vector3>>();

        for (int i = 0; i < vertices.Count; i++)
        {
            var p1 = vertices[i];
            var p2 = vertices[(i + 1) % vertices.Count];
            edges.Add(new Pair<Vector3>(p1, p2));
        }
        
        edges = edges.OrderBy(e => -((e.x - e.z).sqrMagnitude)).ToList();
        
        for (int i = 0; i < edges.Count; i++)
        {
            var p1 = edges[i].x;
            var p2 = edges[i].z;

            var clipPoint1 = ChunkUtil.GetPointProjectionOnLine(centroid, p1, p2);
            var dir = (p1 - p2).normalized;
            var right = new Vector3(dir.z, 0, -dir.x);
            var clipPoint2 = clipPoint1 + right * buildingWidth;

            var s1 = clipPoint1 + dir * w;
            var s2 = clipPoint1 - dir * w;
            var e1 = clipPoint2 + dir * w;
            var e2 = clipPoint2 - dir * w;

            List<IntPoint> buildingPlot;
            remainingPoly = CutBuildingPlots(remainingPoly, s1, s2, e1, e2, out buildingPlot);

            if(buildingPlot != null && ChunkUtil.PolygonArea(buildingPlot) > buildingMinArea)
                buildingPlots.Add(buildingPlot);

            if (remainingPoly == null)
                break;
        }

        return buildingPlots;
    }

    /// <summary>
    /// Cuts the given 'remainingPoly' with a rectangle defined through s1-s2-e2-e1 and returns the cut polygons in buildingPlots list.
    /// </summary>
    /// <param name="remainingPoly"></param>
    /// <param name="s1"></param>
    /// <param name="s2"></param>
    /// <param name="e1"></param>
    /// <param name="e2"></param>
    /// <param name="buildingPlot"></param>
    /// <returns></returns>
    private List<IntPoint> CutBuildingPlots(List<IntPoint> remainingPoly, Vector3 s1, Vector3 s2, Vector3 e1, Vector3 e2, out List<IntPoint> buildingPlot)
    {
        buildingPlot = null;
        var clip = new List<IntPoint>();
        clip.Add(new IntPoint(s1));
        clip.Add(new IntPoint(s2));
        clip.Add(new IntPoint(e2));
        clip.Add(new IntPoint(e1));

        var clipper = new Clipper();
        clipper.AddPath(remainingPoly, PolyType.ptSubject, true);
        clipper.AddPath(clip, PolyType.ptClip, true);

        var housePolys = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, housePolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        //TODO: handle better
        if(housePolys.Count == 1)
        {
            buildingPlot = housePolys[0];
        }
        
        var newPolys = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctDifference, newPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        if (newPolys.Count == 0)
            return null;

        return newPolys[0];
    }
    
    /// <summary>
    /// Determines if the given position with given direction is suitable for a street by determining if it intersects with any nearby side roads or highways.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="streetDir"></param>
    /// <returns></returns>
    private bool IsValidPositionForCityStreet(Vector3 position, Vector3 streetDir)
    {
        if (mesoRoadData.CentralMesoRoadNode == null)
            return true;

        if ((mesoRoadData.CentralMesoRoadNode.GlobalPos - position).sqrMagnitude < 100f)
            return false;

        foreach (var neighbour in mesoRoadData.CentralMesoRoadNode.Neighbours)
        {
            var spline = mesoRoadData.CentralRoadSegments[neighbour].Spline;
            if (IsTooCloseToSpline(position, spline, streetDir))
                return false;
        }

        return true;
    }

    /// <summary>
    /// Determines if the point with the given direction is too close to the spline so it can be dropped from street consideration.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="spline"></param>
    /// <param name="streetDir"></param>
    /// <returns></returns>
    private bool IsTooCloseToSpline(Vector3 position, List<Vector3> spline, Vector3 streetDir)
    {
        for (int i = 0; i < spline.Count; i++)
        {
            if (ChunkUtil.PointDistanceFromLineSegment(position, spline[i], spline[(i + 1) % spline.Count]) < 25)
            {
                //Point is close to spline, also check if they're aligned similarly
                var splineDir = (spline[(i + 1) % spline.Count] - spline[i]).normalized;
                if (Mathf.Abs(ChunkUtil.Angle(Vector3.zero, splineDir, streetDir)) < 35)
                    return true;
            }
        }
        return false;
    }

}
