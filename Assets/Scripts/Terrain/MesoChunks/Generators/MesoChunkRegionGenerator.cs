﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClipperLib;
using UnityEngine;

public class MesoChunkRegionGenerator
{
    private MesoChunk mesoChunk;

    private Point2 mesoChunkPos;

    private MacroChunk macroChunk;

    private MesoRoadData mesoRoadData;
    
    public MesoChunkRegionGenerator(MesoChunk mesoChunk)
    {
        this.mesoChunk = mesoChunk;
        mesoChunkPos = mesoChunk.Pos;
        macroChunk = mesoChunk.MacroChunk;
    }

    /// <summary>
    /// Generates the chunk regions (and their subregions) for the given mesochunk.
    /// </summary>
    /// <param name="roadData">Mesochunk's previously generated road data</param>
    /// <param name="villageRegion">If this mesochunk contains a village, the specific village region is returned via this field.</param>
    /// <returns></returns>
    public List<MesoChunkRegion> GenerateRegionsAsync(MesoRoadData roadData, out MesoChunkRegion villageRegion)
    {
        mesoRoadData = roadData;
        var regions = new List<MesoChunkRegion>();

        var cityData = macroChunk.GetMesoChunkCityData(mesoChunkPos);

        GenerateRuralRegionsAsync(regions, mesoRoadData, out villageRegion);

        return regions;
    }

    /// <summary>
    /// Generates the rural mesochunkregions of the chunk. If chunk contains a village, it is returned in the villageRegion field.
    /// </summary>
    /// <param name="regions"></param>
    /// <param name="roadData"></param>
    /// <param name="villageRegion"></param>
    private void GenerateRuralRegionsAsync(List<MesoChunkRegion> regions, MesoRoadData roadData, out MesoChunkRegion villageRegion)
    {
        var allRuralNodePositions = new List<Vector2>();
        var relevantRuralNodePositions = new List<Vector2>();
        var relevantRuralNodeTypes = new List<RegionType>();
        var relevantRuralNodeChunkPositions = new List<Point2>();
        villageRegion = null;
        var r = 2;
        for (int x = mesoChunkPos.x - r; x <= mesoChunkPos.x + r; x++)
        {
            for (int z = mesoChunkPos.z - r; z <= mesoChunkPos.z + r; z++)
            {
                var neighbourMesoChunkPos = new Point2(x, z);
                var typeNoise = NoiseService.GetRegionTypeNoise(x * Const.MESO_CHUNK_WIDTH_IN_METERS, z * Const.MESO_CHUNK_WIDTH_IN_METERS);

                var type = typeNoise < 0.43f ? RegionType.Cultivation
                    : (typeNoise < 0.69f ? RegionType.Forestry : RegionType.NatureReserve);


                var neighbourChunkPos = new Point2(x, z);
                MesoRoadNode roadNode = null;
                var nodePos = GetRuralNodePosition(neighbourChunkPos, type, out roadNode);
                allRuralNodePositions.Add(nodePos);

                if (CoordUtil.MesoChunkToMacroChunk(neighbourMesoChunkPos) == macroChunk.Pos && IsVillage(macroChunk, roadNode, neighbourMesoChunkPos))
                {
                    //Neighbouring chunk is on this macrochunk so we can use it to determine if it's a village
                    type = RegionType.Village;
                }

                //Village check

                if (neighbourChunkPos.DistOnLongestAxis(mesoChunkPos) < 1)
                {
                    relevantRuralNodeTypes.Add(type);
                    relevantRuralNodePositions.Add(nodePos);
                    relevantRuralNodeChunkPositions.Add(neighbourChunkPos);
                }

            }
        }

        var offsetPos = CoordUtil.MesoChunkToGlobalPosition(mesoChunkPos.x - 1, mesoChunkPos.z - 1);
        var bigRect = new Rect(offsetPos.x, offsetPos.z,
            Const.MESO_CHUNK_WIDTH_IN_METERS * 3, Const.MESO_CHUNK_WIDTH_IN_METERS * 3);
        var voronoi = new Delaunay.Voronoi(allRuralNodePositions, null, bigRect);

        var clipper = new Clipper();

        var terrainPopulator = new MesoChunkTerrainPopulator();
        
        for (int i = 0; i < relevantRuralNodePositions.Count; i++)
        {
            var node = relevantRuralNodePositions[i];

            var voronoiCell = voronoi.Region(node);

            if (voronoiCell.Count == 0)
            {
                continue;
            }

            var pointOutline = new List<IntPoint>();
            
            for (int j = 0; j < voronoiCell.Count; j++)
            {
                var v = new Vector3(voronoiCell[j].x, 0, voronoiCell[j].y);
                pointOutline.Add(new IntPoint(v));
            }


            //Check if intersects with any cities
            clipper.Clear();
            foreach (var city in macroChunk.Data.Cities)
            {
                clipper.AddPath(city.OutterPointOutline, PolyType.ptClip, true);
            }

            clipper.AddPath(pointOutline, PolyType.ptSubject, true);

            var random = ChunkUtil.GetMesoChunkRandom(mesoChunkPos);

            var cityIntersectionPolys = new List<List<IntPoint>>();
            clipper.Execute(ClipType.ctIntersection, cityIntersectionPolys, PolyFillType.pftNonZero,
                PolyFillType.pftNonZero);
            if (cityIntersectionPolys.Count != 0)
            {
                //Cell intersects with city, cull it by city

                var nonCityPolys = new List<List<IntPoint>>();
                clipper.Execute(ClipType.ctDifference, nonCityPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);


                //TODO: Duplicate code with below
                foreach (var _pointOutline in nonCityPolys)
                {
                    CreateRegionAndGenerateRoadMarginRegions(random, clipper, relevantRuralNodeTypes[i], relevantRuralNodeChunkPositions[i], _pointOutline,
                        terrainPopulator, out villageRegion, regions);
                }
                
                break;
            }

            CreateRegionAndGenerateRoadMarginRegions(random, clipper, relevantRuralNodeTypes[i], relevantRuralNodeChunkPositions[i], pointOutline,
                terrainPopulator, out villageRegion, regions);
        }
    }

    /// <summary>
    /// Creates a region for the given pointOUtline with the given type and also generates road margin polygons aroound any roads it intersects.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="clipper"></param>
    /// <param name="type"></param>
    /// <param name="regionMesoChunkPos"></param>
    /// <param name="pointOutline"></param>
    /// <param name="terrainPopulator"></param>
    /// <param name="villageRegion"></param>
    /// <param name="regions"></param>
    private void CreateRegionAndGenerateRoadMarginRegions(System.Random random, Clipper clipper, RegionType type, Point2 regionMesoChunkPos, List<IntPoint> pointOutline, 
        MesoChunkTerrainPopulator terrainPopulator, out MesoChunkRegion villageRegion, List<MesoChunkRegion> regions)
    {
        villageRegion = null;
        var region = CreateRegion(random, type, pointOutline, terrainPopulator);
        if (region.Type == RegionType.Village && regionMesoChunkPos == mesoChunkPos)
        {
            villageRegion = region;
        }

        regions.Add(region);

        //Finally create an empty region for the road margins
        clipper.Clear();
        clipper.AddPath(pointOutline, PolyType.ptSubject, true);
        clipper.AddPaths(mesoRoadData.ShoulderPolys, PolyType.ptClip, true);

        var roadMarginPolys = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, roadMarginPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

//        foreach (var shoulderPoly in mesoRoadData.ShoulderPolys)
//        {
//            var collection = shoulderPoly;
//            for (int m = 0; m < collection.Count; m++)
//            {
//                var d1 = collection[m];
//                var d2 = collection[(m + 1) % collection.Count];
//                DebugRenderer.Add(d1, d2, Color.red);
//            }
//        }

        foreach (var roadMarginPoly in roadMarginPolys)
        {
            var roadMarginRegion = new MesoChunkRegion(RegionType.Plain, roadMarginPoly, ClipperUtil.ToVectors(roadMarginPoly), true);
            regions.Add(roadMarginRegion);

            var collection = roadMarginPoly;
            for (int m = 0; m < collection.Count; m++)
            {
                var d1 = collection[m];
                var d2 = collection[(m + 1) % collection.Count];
                DebugRenderer.Add(d1, d2, Color.magenta);
            }
        }
    }

    /// <summary>
    /// Determines if the given mesoChunk contains a village.
    /// </summary>
    /// <param name="macroChunk"></param>
    /// <param name="roadNode"></param>
    /// <param name="mesoChunkPos"></param>
    /// <returns></returns>
    public static bool IsVillage(MacroChunk macroChunk, MesoRoadNode roadNode, Point2 mesoChunkPos)
    {
        if (roadNode != null && macroChunk.GetMesoChunkCityData(mesoChunkPos) == null)
        {
            //If the node was placed based on road node, override the type of the cell in some cases
            var edges = roadNode.Neighbours.Count;

            if (edges == 1)
            {
                //Always a village
                return true;
            }
            else if (edges > 2)
            {
                //Sometimes a village
                var noise = NoiseService.GetNoiseAtPos(mesoChunkPos.x * -10000f, mesoChunkPos.z * -10000f);
                if (noise < 0.4f)
                    return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Creates a mesochunkregion with the given type and outline. If the region is cultivation, forestry or naturereserve, it is also further subdivided and populated with plants.
    /// </summary>
    /// <param name="random"></param>
    /// <param name="type"></param>
    /// <param name="pointOutline"></param>
    /// <param name="terrainPopulator"></param>
    /// <returns></returns>
    private MesoChunkRegion CreateRegion(System.Random random, RegionType type, List<IntPoint> pointOutline, MesoChunkTerrainPopulator terrainPopulator)
    {
        var outline = ClipperUtil.ToVectors(pointOutline);
        var region = new MesoChunkRegion(type, pointOutline, outline, true);
        
        if (region.Type == RegionType.Cultivation || region.Type == RegionType.Forestry || region.Type == RegionType.NatureReserve)
        {
            SubdivideRegion(region);

            foreach (var subregion in region.SubRegions)
            {
                if (region.Type == RegionType.Forestry && subregion.PointOutline.Count > 5 && random.NextDouble() < 0.3f)
                {
                    //Very-very likely that this region is next to a road because of a high number of outline points - lets place a house in it.
                    TryPlaceHouseInSubregion(subregion);
                }

                var minDistFromBorder = 1.2f;
                
                if (subregion.Type.IsForest() && subregion.Outline.Count > 5)
                {
                    //For forests that are likely next to a road, increase the min dist from border
                    //so large trees dont get in the way of utility poles
                    minDistFromBorder = 4f;
                }

                terrainPopulator.GenerateSubregionTerrainObjectsAsync(mesoChunk, subregion, minDistFromBorder);
            }
        }
        
        return region;
    }

    /// <summary>
    /// Attempts to place a house inside the given subregion.
    /// </summary>
    /// <param name="subregion"></param>
    private void TryPlaceHouseInSubregion(MesoChunkRegion subregion)
    {
        for (int i = 0; i < subregion.Outline.Count; i++)
        {
            var v1 = subregion.Outline[i];
            var v2 = subregion.Outline[(i + 1) % subregion.Outline.Count];
            var v3 = subregion.Outline[(i + 2) % subregion.Outline.Count];

            var angleDiff = Mathf.Abs(ChunkUtil.Angle(Vector3.zero, v3 - v2, v2 - v1));

            //If the angle differs very little for two consequential segments, its highly likely that its the side with the road.
            if (angleDiff < 10 && (v3 - v2).magnitude > 20)
            {
                //Edge is at least 20m long

                var dir = (v2 - v3).normalized;
                var right = new Vector3(dir.z, 0, -dir.x);

                var distFromRegionBorder = 21f;

                var gardenPos = v2 + (v3 - v2) * 0.5f + right * distFromRegionBorder;
                var houseRotationRad = Mathf.Atan2(right.z, right.x);

                var gardenWidth = 40f;

                List<IntPoint> gardenPointOutline;
                if (ChunkUtil.IsValidGardenPosition(gardenPos, houseRotationRad, gardenWidth, 60, subregion.Outline, out gardenPointOutline))
                {
                    
                    var houseYoffset = Mathf.Abs((v2.x + v2.z) * 3739) % (gardenWidth - 12) - (gardenWidth - 12) / 2;

                    var housePos = v2 + (v3 - v2) * 0.5f + right * 8.5f - dir * houseYoffset;
                    housePos.y = mesoChunk.GetHeightAt(housePos);

                    var housePrefabs = AssetManager.Ins.HousePrefabs;
                    var modelIndex = Mathf.Abs(ChunkUtil.Hash((int)(v2.x), (int)(v2.z) * 782011) % housePrefabs.Count);
                    var obj = TerrainObjectMetaData.Generate(housePrefabs[modelIndex], housePos,
                        180 - houseRotationRad * Mathf.Rad2Deg, 1, 0, 0);

                    var terrainObjects = new List<TerrainObjectMetaData> {obj};

                    var gardenOutline = ClipperUtil.ToVectors(gardenPointOutline);

                    var gatePos = housePos - right * 8.5f;
                    gatePos.y = 0;

                    ChunkUtil.GenerateFenceTerrainObjects(mesoChunk, gardenOutline, terrainObjects, gatePos, 1.7f);

                    subregion.SetTerrainObjectMetaDatas(terrainObjects);

                    var houseOutline = ChunkUtil.GetHouseOutline(housePos, houseRotationRad, 14, 14);

                    var ignorableOutlines = ConstructGardenIgnorableOutlines(gardenOutline, 3);
                    ignorableOutlines.Add(houseOutline);
                    subregion.SetIgnorableOutlines(ignorableOutlines);

                    return;
                }

            }
        }
    }

    /// <summary>
    /// Constructs a thick polygon around the given gardenoutline which can be passed to ignorableoutlines of a mesochunkregion so it will not generate plants too close to the garden.
    /// </summary>
    /// <param name="gardenOutline"></param>
    /// <param name="width">radius of the polygon generated around the garden polygon</param>
    /// <returns></returns>
    private List<List<Vector3>> ConstructGardenIgnorableOutlines(List<Vector3> gardenOutline, float width)
    {
        var ignorableOutlines = new List<List<Vector3>>();

        for (int i = 0; i < gardenOutline.Count; i++)
        {
            var v1 = gardenOutline[i];
            var v2 = gardenOutline[(i + 1) % gardenOutline.Count];

            var fwd = (v2 - v1);
            fwd.y = 0;
            fwd = fwd.normalized;
            var right = new Vector3(fwd.z, 0, -fwd.x);

            var ignorableOutline = new List<Vector3>();

            ignorableOutline.Add(v1 + right * width - fwd * width * 0.5f);
            ignorableOutline.Add(v1 - right * width - fwd * width * 0.5f);
            ignorableOutline.Add(v2 - right * width + fwd * width * 0.5f);
            ignorableOutline.Add(v2 + right * width + fwd * width * 0.5f);

            ignorableOutlines.Add(ignorableOutline);
            
        }

        return ignorableOutlines;
    }

    /// <summary>
    /// Subdivides the region into subregions and assigns them subtypes based on the type of the main region.
    /// </summary>
    /// <param name="region">Region to be subdivided.</param>
    private void SubdivideRegion(MesoChunkRegion region)
    {
        var subregions = new List<MesoChunkRegion>();
        //TODO: Could reuse clipper from caller
        Clipper clipper = new Clipper();

        clipper.AddPath(region.PointOutline, PolyType.ptSubject, true);
        clipper.AddPaths(mesoRoadData.ShoulderPolys, PolyType.ptClip, true);

        var roadClippedPolys = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctDifference, roadClippedPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        foreach (var roadClippedPoly in roadClippedPolys)
        {
            Subdivide(region, subregions, clipper, roadClippedPoly, 4, 25000);
        }

        region.SetSubRegions(subregions);
    }

    /// <summary>
    /// Recursive function for subdividing the given 'pointOutline' until a specific recursion depth is reached.
    /// </summary>
    /// <param name="region">The region that is being subdivided.</param>
    /// <param name="subregions">List which stores the generated subregions once recursion depth is reached.</param>
    /// <param name="clipper">Reused clipper instance to help performance.</param>
    /// <param name="pointOutline">The outline of the polygon being divided.</param>
    /// <param name="depth">Current depth of recursion. Stops at 0.</param>
    /// <param name="minArea">The min area of a subregion which will also stop recursion in square meters.</param>
    private void Subdivide(MesoChunkRegion region, List<MesoChunkRegion> subregions, Clipper clipper, List<IntPoint> pointOutline, int depth, float minArea)
    {
        var outline = ClipperUtil.ToVectors(pointOutline);
        var centroid = ChunkUtil.CentroidOfPolygon(outline);
        if (depth == 0 || ChunkUtil.PolygonArea(pointOutline) < minArea)
        {
            var type = GetSubRegionType(region, centroid, outline);
            subregions.Add(new MesoChunkRegion(type, pointOutline, outline, true));
            return;
        }

        var longestEdgeI = ChunkUtil.LongestEdge(pointOutline);

        var start = outline[longestEdgeI];
        var end = outline[(longestEdgeI + 1) % outline.Count];

        var clipPoint1 = ChunkUtil.GetPointProjectionOnLine(centroid, start, end);

        var clipPoint2 = centroid;
        
        CutPoly(region, subregions, clipper, pointOutline, clipPoint1, clipPoint2, depth, minArea);
    }

    /// <summary>
    /// Continuation of recursion which cuts the given polygon 'points' with a line through 'start' and 'end'.
    /// </summary>
    /// <param name="region">The region that is being subdivided.</param>
    /// <param name="subregions">List which stores the generated subregions once recursion depth is reached.</param>
    /// <param name="clipper">Reused clipper instance to help performance.</param>
    /// <param name="pointOutline">The outline of the polygon being divided.</param>
    /// <param name="start">start point of the cut line</param>
    /// <param name="end">end point of the cut line</param>
    /// <param name="depth">Current depth of recursion. Stops at 0.</param>
    /// <param name="minArea">The min area of a subregion which will also stop recursion in square meters.</param>
    private void CutPoly(MesoChunkRegion region, List<MesoChunkRegion> subregions, Clipper clipper, List<IntPoint> pointOutline, Vector3 start, Vector3 end, int depth, float minArea)
    {
        var dir = (start - end).normalized;
        var right = new Vector3(-dir.z, 0, dir.x);

        var w = 2000f;
        var s1 = start + dir * w;
        var s2 = end - dir * w;
        var e1 = s1 + right * w * 1.42f;
        var e2 = s2 + right * w * 1.42f;

        var clip = new List<IntPoint>();
        clip.Add(new IntPoint(s1));
        clip.Add(new IntPoint(s2));
        clip.Add(new IntPoint(e2));
        clip.Add(new IntPoint(e1));
        
        clipper.Clear();
        clipper.AddPath(pointOutline, PolyType.ptSubject, true);
        clipper.AddPath(clip, PolyType.ptClip, true);

        var poly1s = new List<List<IntPoint>>();
        var poly2s = new List<List<IntPoint>>();
        clipper.Execute(ClipType.ctIntersection, poly1s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);
        clipper.Execute(ClipType.ctDifference, poly2s, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

        if (poly1s.Count > 0)
        {
            foreach (var poly1 in poly1s)
            {
                Subdivide(region, subregions,clipper, poly1, depth - 1, minArea);
            }
        }

        if (poly2s.Count > 0)
        {
            foreach (var poly2 in poly2s)
            {
                Subdivide(region, subregions, clipper, poly2, depth - 1, minArea);
            }
        }
    }

    /// <summary>
    /// Gets the type of a subregion using the parent region's type. E.g. forestry region gets subdivided into more specific types of forests.
    /// </summary>
    /// <param name="parentRegion">the parent region with a more generic type such as forestry or cultivation</param>
    /// <param name="outline">The polygon of the subregion which is used to evaluate the choices.</param>
    /// <returns>The type of the subregion.</returns>
    private RegionType GetSubRegionType(MesoChunkRegion parentRegion, Vector3 subregionCentroid, List<Vector3> outline)
    {

        var noise = NoiseService.GetTerrainObjectNoise(subregionCentroid);

        if (parentRegion.Type == RegionType.Forestry)
        {
            if (noise < 0.5f)
            {
                return (int)(noise * 51031) % 5 < 2
                    ? RegionType.SubConiferousForestPlanted
                    : ((int)(noise * 51031) % 5 < 4 ? RegionType.SubDeciduousForestPlanted : RegionType.SubResidenceWithTreesAndBushes);
            }
            else
            {
                //Natural
                return (int)(noise * 51031)  % 3 == 0
                    ? RegionType.SubConiferousForestNatural
                    : ((int)(noise * 51031) % 3 == 1 ? RegionType.SubDeciduousForestNatural : RegionType.SubMixedForest);
            }


        }

        if (parentRegion.Type == RegionType.Cultivation)
        {
            if (outline.Count > 5 && (int)(noise * 10000) % 10 == 0)
            {
                //Likely next to a road
                return RegionType.SubResidenceWithTreesAndBushes;
            }

            if ((int) (noise * 10000) % 20 < 4)
            {
                //Tiny piece of forest
                var val = (int) (noise * 10000) % 20;

                return val == 0 ? RegionType.SubConiferousForestNatural : (val == 1 ? RegionType.SubDeciduousForestNatural : RegionType.SubMixedForest);
            }

            if (noise < 0.48)
            {
                return RegionType.SubFarmBig;
            } else if(noise < 0.6) {
                return RegionType.SubFarmSmall;
            }
            else
            {
                return RegionType.SubFarmEmpty;
            }
    }

        return parentRegion.Type;
    }
    
    /// <summary>
    /// Returns the position of the rural node on a neighbouring chunk using the pregenerated type of neighbouring chunk and the initial mesochunk's roadData.
    /// If the rural node is placed based on a road node, the corresponding road node is also returned in the roadNode field.
    /// </summary>
    /// <param name="neighbourChunkPos">Position of the mesochunk near generating mesochunk.</param>
    /// <param name="type">Pregenerated type of the neighbouring mesochunk.</param>
    /// <param name="roadNode">the corresponding roadnode based on which the rural node was placed, if one was used.</param>
    /// <returns></returns>
    private Vector2 GetRuralNodePosition(Point2 neighbourChunkPos, RegionType type,  out MesoRoadNode roadNode)
    {
        roadNode = null;
        //Case 1: road node on it
        //Note that mesoroadnodes only contain 5x5 window but we need 7x7 so for some we do an additional further distance check
        if (mesoRoadData.MesoRoadNodes.ContainsKey(neighbourChunkPos))
        {
            var pos = mesoRoadData.MesoRoadNodes[neighbourChunkPos].GlobalPos;
            roadNode = mesoRoadData.MesoRoadNodes[neighbourChunkPos];
            return new Vector2(pos.x, pos.z);
        } else if(neighbourChunkPos.DistOnLongestAxis(mesoChunkPos) > 2)
        {
            var pos = MacroChunk.GetMesoChunkRoadGlobalPosition(neighbourChunkPos);
            if (pos.HasValue)
            {
                return new Vector2(pos.Value.x, pos.Value.z);
            }
        }
        
        //Case 2:
        if (type == RegionType.Cultivation && NoiseService.GetNoiseAtPos(neighbourChunkPos.x * 7500f, neighbourChunkPos.z * 7500f) < 0.5f)
        {
            //Fixed placement
            var chunkRuralTile =
                new Point2(Const.MESO_CHUNK_WIDTH_IN_TILES / 2, Const.MESO_CHUNK_WIDTH_IN_TILES / 2) +
                neighbourChunkPos.Scl(Const.MESO_CHUNK_WIDTH_IN_TILES);
            var pos = CoordUtil.GlobalTileToGlobalPosition(chunkRuralTile);
            return new Vector2(pos.x, pos.z);
        }
        else
        {
            //Random placement
            var chunkRuralTile = ChunkUtil.GetMesoChunkRuralNodeTile(neighbourChunkPos.x, neighbourChunkPos.z);
            var pos = CoordUtil.GlobalTileToGlobalPosition(chunkRuralTile);
            return new Vector2(pos.x, pos.z);
        }
    }

}
