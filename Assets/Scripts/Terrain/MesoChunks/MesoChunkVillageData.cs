﻿using System;
using System.Collections.Generic;
using ClipperLib;

public struct VillageArea
{
    public List<IntPoint> Outline;
    
    public VillageArea(List<IntPoint> outline)
    {
        Outline = outline;
    }
}

public class MesoChunkVillageData {

    public MesoChunkRegion VillageRegion { get; private set; }

    public List<VillageArea> VillageAreas { get; private set; }

    public String Name { get; private set; }

    public MesoChunkVillageData(MesoChunk mesoChunk, MesoChunkRegion villageRegion, List<VillageArea> villageAreas)
    {
        
        VillageRegion = villageRegion;
        VillageAreas = villageAreas;
        Name = SettlementNameGenerator.GenerateName(CoordUtil.MesoChunkToGlobalPosition(mesoChunk.Pos));
    }
}
