﻿using System.Collections.Generic;

public class MesoChunkData {

    public float[] Noise { get; private set; }

    public MesoRoadData RoadData { get; private set; }

    public bool ContainsHighway { get; private set; }

    public List<MesoChunkRegion> Regions { get; private set; }

    public List<MesoChunkRegion> RuralRegions { get; private set; }
    
    public MesoChunkCityData CityData { get; private set; }

    public MesoChunkVillageData VillageData { get; private set; }

    public MesoChunkData(float[] noise, MesoRoadData roadData, bool containsHighway, List<MesoChunkRegion> regions, MesoChunkCityData cityData, MesoChunkVillageData villageData)
    {
        Noise = noise;
        RoadData = roadData;
        ContainsHighway = containsHighway;
        Regions = regions;
        CityData = cityData;
        VillageData = villageData;

        RuralRegions = new List<MesoChunkRegion>();

        foreach (var region in regions)
        {
            if (region.Type == RegionType.Cultivation || region.Type == RegionType.Forestry 
                 || region.Type == RegionType.NatureReserve || region.Type == RegionType.Village)
            {
                RuralRegions.Add(region);
            }
        }
    }

    public float GetNoiseForLocalTile(Point2 localTile) {
        return GetNoiseForLocalTile(localTile.x, localTile.z);
    }

    public float GetNoiseForLocalTile(int localTileX, int localTileZ) {
        return Noise[localTileZ * (Const.MESO_CHUNK_WIDTH_IN_TILES + 1) + localTileX];
    }

}
