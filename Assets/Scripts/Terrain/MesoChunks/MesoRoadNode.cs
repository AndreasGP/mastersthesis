﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MesoRoadNode
{


    public Point2 MesoChunkPos { get; private set; }

    public Vector3 GlobalPos { get; private set; }

    public List<Vector3> Neighbours { get; private set; }

    /// <summary>
    /// /A collection of road pairs that should be continuous through this road node.
    /// </summary>
    public Dictionary<Vector3, Vector3> ContinuousRoads { get; private set; }

    public MesoRoadNode(Point2 mesoChunkPos, Vector3 globalPos, List<Vector3> neighbours, Dictionary<Vector3, Vector3> continuousRoads)
    {
        MesoChunkPos = mesoChunkPos;
        GlobalPos = globalPos;
        Neighbours = neighbours;
        ContinuousRoads = continuousRoads;
    }

    /// <summary>
    /// If the number of neighbours is uneven, returns the first road that does not have a ContinuousRoads entry, otherwise null.
    /// </summary>
    /// <returns></returns>
    public Vector3? GetNonContinuousRoad()
    {
        foreach (var neighbour in Neighbours)
        {
            if (!ContinuousRoads.ContainsKey(neighbour))
                return neighbour;
        }

        return null;
    }

}