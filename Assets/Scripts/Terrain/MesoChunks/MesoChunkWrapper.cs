﻿using System;
using UnityEngine;

public class MesoChunkWrapper : MonoBehaviour {

    public MesoChunk Chunk { get; private set; }

    public Action ChunkInitialized;

    public void Initialize(MesoChunk chunk)
    {
        Chunk = chunk;
        name = "MesoChunk " + chunk.Pos;
        if(ChunkInitialized != null)
            ChunkInitialized();
    }
    

}
