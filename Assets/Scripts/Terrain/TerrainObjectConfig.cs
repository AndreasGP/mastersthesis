﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public struct TerrainObjectConfig
{
    public GameObject Prefab { get; private set; }
    
    public float Probability { get; private set; }

    public float MinScale { get; private set; }

    /// <summary>
    /// Scale will be bound to [MinScale, MinScale + ScaleVariance]
    /// </summary>
    public float ScaleVariance { get; private set; }

    /// <summary>
    /// Model is offset downwards by this amount
    /// </summary>
    public float HeightOffset { get; private set; }

    public TerrainObjectConfig(GameObject prefab, float probability, float minScale, float scaleVariance, float heightOffset) : this()
    {
        Prefab = prefab;
        Probability = probability;
        MinScale = minScale;
        ScaleVariance = scaleVariance;
        HeightOffset = heightOffset;
    }
}