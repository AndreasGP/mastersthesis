﻿using System.Collections;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public class GridSampler : ISampler
{
    private float angle;
    
    private float distX;
    private float distZ;

    private float varianceX;
    private float varianceZ;

    //Transformed poly
    private List<Vector3> poly;

    private Rect rect;
    
    public GridSampler(List<Vector3> originalPoly, float distX, float distZ, float varianceX, float varianceZ, float angle)
    {
        this.distX = distX;
        this.distZ = distZ;
        this.varianceX = varianceX;
        this.varianceZ = varianceZ;
        this.angle = angle;

        poly = Translate(originalPoly, angle);

        rect = ChunkUtil.GetPointsRect(poly);
    }

    /// Return a lazy sequence of samples. You typically want to call this in a foreach loop, like so:
    ///   foreach (Vector2 sample in sampler.Samples()) { ... }
    public IEnumerable<Vector3> Samples()
    {
        var useVariance = varianceX > 0 || varianceZ > 0;

        var rand = useVariance ? new System.Random((int) (poly[0].z * 51043)) : null;

        for (float x = rect.xMin + distX/2; x < rect.xMax; x += distX)
        {
            for (float z = rect.yMin + distZ / 2; z < rect.yMax; z += distZ)
            {
                var pos = new Vector3(x, 0, z);

                if (useVariance)
                {
                    pos.x += (float) (rand.NextDouble() - 0.5) * 2 * varianceX;
                    pos.z += (float) (rand.NextDouble() - 0.5) * 2 * varianceZ;
                }

                if (ChunkUtil.InsideConvexPolygon(pos, poly))
                {
                    yield return Translate(pos, -angle);
                }
            }
        }
    }

    public IEnumerable<Vector3> SecondarySamples()
    {
        var useVariance = varianceX > 0 || varianceZ > 0;

        var rand = new System.Random((int)(poly[0].z * 51043));

        var xCount = 0;
        var zCount = 0;

        for (float x = rect.xMin + distX / 2; x < rect.xMax; x += distX / 2)
        {
            xCount++;
            for (float z = rect.yMin + distZ / 2; z < rect.yMax; z += distZ / 2)
            {
                zCount++;

                if(xCount % 2 == 1 && zCount % 2 == 1 || rand.NextDouble() < 0.45f)
                    continue;

                var pos = new Vector3(x, 0, z);

                if (useVariance)
                {
                    pos.x += (float)(rand.NextDouble() - 0.5) * 2 * varianceX;
                    pos.z += (float)(rand.NextDouble() - 0.5) * 2 * varianceZ;
                }

                if (ChunkUtil.InsideConvexPolygon(pos, poly))
                {
                    yield return Translate(pos, -angle);
                }
            }
        }
    }

    private List<Vector3> Translate(List<Vector3> points, float angle)
    {
        var output = new List<Vector3>();

        foreach (var point in points)
        {
            output.Add(Translate(point, angle));
        }

        return output;
    }

    private Vector3 Translate(Vector3 point, float angle)
    {
        return new Vector3(point.x * Mathf.Cos(angle) - point.z * Mathf.Sin(angle),
            0, point.z * Mathf.Cos(angle) + point.x * Mathf.Sin(angle));
    }

}
