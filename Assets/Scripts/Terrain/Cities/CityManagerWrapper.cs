﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CityManagerWrapper : MonoBehaviour
{
    public CityWrapper CityPrefab;

    private CityManager cityManager;

    private Dictionary<Point2, CityWrapper> cities;
    
    void Awake() {
        cities = new Dictionary<Point2, CityWrapper>();

        cityManager = new CityManager();
        cityManager.OnCityOutlineFinishedAsync += OnCityOutlineFinishedAsync;
    }
    
    private void OnCityOutlineFinishedAsync(City city)
    {
        ActionQueue.Ins.Enqueue(() => OnCityOutlineFinished(city));
    }

    private void OnCityOutlineFinished(City city) {
        var wrapper = Instantiate(CityPrefab);
        wrapper.Initialize(city);
        cities.Add(city.MacroChunkPos, wrapper);
    }
}
