﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public class StreetSegment {


    public List<IntPoint> PointOutline { get; private set; }

    public List<Vector3> Vertices { get; private set; }

    public Vector3 Left { get; private set; }

    public Vector3 Right { get; private set; }

    public bool UseDirtRoadTexture { get; private set; }

    public StreetSegment(List<IntPoint> pointOutline, List<Vector3> vertices, Vector3 left, Vector3 right, bool useDirtRoadTexture)
    {
        PointOutline = pointOutline;
        Vertices = vertices;
        Left = left;
        Right = right;
        UseDirtRoadTexture = useDirtRoadTexture;
    }
}
