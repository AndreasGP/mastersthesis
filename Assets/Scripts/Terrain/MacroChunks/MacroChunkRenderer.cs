﻿using System;
using UnityEngine;

[RequireComponent(typeof(MacroChunkWrapper))]
public partial class MacroChunkRenderer : MonoBehaviour
{

    [SerializeField] private MeshFilter terrainMeshFilter;

    private MacroChunkWrapper macroChunkWrapper;

    private MacroChunk macroChunk;

    private void Awake()
    {
        macroChunkWrapper = GetComponent<MacroChunkWrapper>();
        macroChunkWrapper.ChunkInitialized += ChunkInitialized;
    }

    void ChunkInitialized()
    {
        macroChunk = macroChunkWrapper.Chunk;

        if (Settings.GenerateMacroChunkNoiseMesh)
        {

            ThreadPool.QueueUserWorkItem((obj) =>
            {
                try
                {
                    GenerateChunkTerrainMeshAsync();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }, null);
        }
    }

    private void GenerateChunkTerrainMeshAsync()
    {
        var width = Const.MACRO_CHUNK_WIDTH_IN_MICRO_CHUNKS;

        var vertices = new Vector3[(width + 1) * (width + 1)];
        var uvs = new Vector2[(width + 1) * (width + 1)];
        var indices = new int[width * width * 6];
        var colors = new Color[(width + 1) * (width + 1)];

        var offset = CoordUtil.MacroChunkToMicroChunk(macroChunk.Pos);

        for (int x = 0; x < width + 1; x++)
        {
            for (int z = 0; z < width + 1; z++)
            {
                var i = z * (width + 1) + x;
                var microChunkPos = CoordUtil.MicroChunkToGlobalPosition(offset.x + x, offset.z + z);

                var type = RegionType.City;

                if (!macroChunk.InsideCity(microChunkPos))
                {

                    var typeNoise = NoiseService.GetRegionTypeNoise(microChunkPos.x, microChunkPos.z);
                    type = typeNoise < 0.43f
                        ? RegionType.Cultivation
                        : (typeNoise < 0.69f ? RegionType.Forestry : RegionType.NatureReserve);
                }

                var vertex = microChunkPos;
                var noise = NoiseService.GetTerrainNoiseAtPos(microChunkPos.x, microChunkPos.z);
                vertex.y = noise * Const.HEIGHTMAP_RANGE_IN_METERS;

                vertices[i] = vertex;
                uvs[i] = new Vector2(x * 16f, z * 16f);
                colors[i] = MesoTerrainMeshBuilder.GetVertexColor(type);
            }
        }

        for (int y = 0; y < width; y++)
        {
            for (int x = 0; x < width; x++)
            {
                var i = y * (width) + x;
                var j = y * (width + 1) + x;

                indices[i * 6] = j;
                indices[i * 6 + 1] = j + width + 1;
                indices[i * 6 + 2] = j + 1;

                indices[i * 6 + 3] = j + 1;
                indices[i * 6 + 4] = j + width + 1;
                indices[i * 6 + 5] = j + 1 + width + 1;
            }
        }

        var meshBuilder = new MeshBuilder();
        meshBuilder.AddIndices(0, indices);
        meshBuilder.AddVertices(vertices);
        meshBuilder.AddTexCoords(uvs);
        meshBuilder.AddColors(colors);
        
        ActionQueue.Ins.EnqueueHeavy(() => GenerateTerrainMesh(meshBuilder));
    }


    private void GenerateTerrainMesh(MeshBuilder meshBuilder)
    {
        if (this == null)
        {
            return;
        }

        //TODO: Not generate with an offset
        terrainMeshFilter.transform.localPosition = new Vector3(0, -8, 0);

        var mesh = meshBuilder.ToMesh();

        if (mesh != null)
        {
            terrainMeshFilter.sharedMesh = mesh;
            terrainMeshFilter.sharedMesh.name = "Terrain Mesh";
            terrainMeshFilter.sharedMesh.RecalculateNormals();

            var renderer = terrainMeshFilter.gameObject.GetComponent<MeshRenderer>();

            var mat = renderer.material;

            var mats = new Material[mesh.subMeshCount];
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i] = mat;
            }

            renderer.materials = mats;
        }
    }

}
