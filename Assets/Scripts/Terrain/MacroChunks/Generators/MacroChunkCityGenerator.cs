﻿using System.Collections.Generic;
using System.Threading;
using ClipperLib;
using UnityEngine;

public class MacroChunkCityGenerator
{

    private MacroChunk macroChunk;

    private Point2 macroChunkPos;

    private MacroRoadData macroRoadData;

    public MacroChunkCityGenerator(MacroChunk macroChunk, MacroRoadData macroRoadData)
    {
        this.macroChunk = macroChunk;
        macroChunkPos = macroChunk.Pos;
        this.macroRoadData = macroRoadData;
    }

    public List<City> GenerateCitiesAsync()
    {
        var cities = new List<City>();

        var waitingForCities = new List<Point2>();

        for (int x = macroChunkPos.x - 1; x <= macroChunkPos.x + 1; x++)
        {
            for (int z = macroChunkPos.z - 1; z <= macroChunkPos.z + 1; z++)
            {
                var pos = new Point2(x, z);
                if (macroRoadData.MacroRoadNodes.ContainsKey(pos))
                {
                    var neighbourRoadNode = macroRoadData.MacroRoadNodes[pos];

                    if (CityManager.Ins.WillContainCity(pos, neighbourRoadNode))
                    {
                        bool generating;
                        var city = CityManager.Ins.GenerateCityOutlineAsync(pos, neighbourRoadNode, out generating);
                        if (generating)
                        {
                            waitingForCities.Add(pos);
                        }

                        if (city != null && city.Rect.Overlaps(macroChunk.Rect))
                        {
                            cities.Add(city);
                        }
                    }
                }
            }
        }
        
        while (waitingForCities.Count > 0)
        {
            for (int i = waitingForCities.Count - 1; i >= 0; i--)
            {
                var pos = waitingForCities[i];
                var roadNode = macroRoadData.MacroRoadNodes[pos];

                bool generating;
                var city = CityManager.Ins.GenerateCityOutlineAsync(pos, roadNode, out generating);

                if (!generating) {
                    waitingForCities.RemoveAt(i);
                    if (city != null && city.Rect.Overlaps(macroChunk.Rect)) {
                        cities.Add(city);
                    }
                }
            }
            Thread.Sleep(1);
        }

        return cities;
    }

    public MesoChunkCityData[] CalculatePerMesoChunkRegionsAsync(List<City> cities)
    {
        var clipper = new Clipper();

        var w = Const.MACRO_CHUNK_WIDTH_IN_MESO_CHUNKS;
        var mesoChunkCities = new MesoChunkCityData[w * w];

        var minMesoChunk = CoordUtil.MacroChunkToMesoChunk(macroChunkPos);
        for (int x = 0; x < w; x++)
        {
            for (int z = 0; z < w; z++)
            {
                var mesoChunkPos = new Point2(minMesoChunk.x + x, minMesoChunk.z + z);
                var mesoRect = ChunkUtil.GetMesoChunkRect(mesoChunkPos);

                foreach (var city in cities)
                {
                    if (city.Rect.Overlaps(mesoRect))
                    {
                        clipper.Clear();
                        clipper.AddPath(ChunkUtil.GetRectAsIntPoints(mesoRect), PolyType.ptSubject, true);
                        clipper.AddPath(city.OutterPointOutline, PolyType.ptClip, true);

                        var cityPolys = new List<List<IntPoint>>();
                        clipper.Execute(ClipType.ctIntersection, cityPolys, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

                        if (cityPolys.Count != 0)
                        {
                            var i = z * w + x;
                            var onlyCity = MesoChunkOnlyContainsCity(cityPolys, mesoRect);
                            mesoChunkCities[i] = new MesoChunkCityData(city, cityPolys, onlyCity);
                        }

                        break;
                    }
                }

            }
        }

        return mesoChunkCities;
    }

    private bool MesoChunkOnlyContainsCity(List<List<IntPoint>> cityPolys, Rect chunkRect)
    {
        if (cityPolys.Count != 1 || cityPolys[0].Count != 4)
        {
            //Early return in 90%+ of cases
            return false;
        }

        var v1 = cityPolys[0][0];
        var v2 = cityPolys[0][1];
        var v3 = cityPolys[0][2];
        var v4 = cityPolys[0][3];

        return (v1.X == v2.X && v3.X == v4.X && v1.Y == v3.Y && v2.Y == v4.Y) ||
               (v1.X == v3.X && v2.X == v4.X && v1.Y == v2.Y && v3.Y == v4.Y);
    }
}
