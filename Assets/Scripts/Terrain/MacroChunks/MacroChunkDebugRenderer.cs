﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[RequireComponent(typeof(MacroChunkWrapper))]
public partial class MacroChunkDebugRenderer : MonoBehaviour {

    private static Color chunkBorderColor = new Color(146/256f, 183/256f, 17/256f);
    private static Color riverColor = new Color(0f, 0f, 0.75f);
    private static Color roadColor = new Color(64/256f, 32/256f, 99/256f);

    private MacroChunkWrapper macroChunkWrapper;

    private MacroChunk macroChunk;

    private void Awake()
    {
        macroChunkWrapper = GetComponent<MacroChunkWrapper>();
        macroChunkWrapper.ChunkInitialized += ChunkInitialized;
    }

    void ChunkInitialized()
    {
        macroChunk = macroChunkWrapper.Chunk;
    }

    void OnDrawGizmos()
    {
        DrawGizmos(SettingsLevel.Always, transform.parent.position);
    }

    void OnDrawGizmosSelected()
    {
        DrawGizmos(SettingsLevel.WhenSelected, transform.parent.position);
    }

    private void DrawGizmos(SettingsLevel level, Vector3 basePosition)
    {
        if (macroChunk == null || !macroChunk.Ready || !Application.isPlaying)
            return;

        if (Settings.DrawMacroChunkBorders == level) {
            Gizmos.color = chunkBorderColor;
            var start = basePosition + CoordUtil.MacroChunkToGlobalPosition(macroChunk.Pos);
            var end = basePosition + CoordUtil.MacroChunkToGlobalPosition(macroChunk.Pos + new Point2(1, 1));
            Gizmos.DrawLine(start, start + new Vector3(Const.MACRO_CHUNK_WIDTH_IN_METERS, 0, 0));
            Gizmos.DrawLine(start, start + new Vector3(0, 0, Const.MACRO_CHUNK_WIDTH_IN_METERS));
            Gizmos.DrawLine(end, end - new Vector3(Const.MACRO_CHUNK_WIDTH_IN_METERS, 0, 0));
            Gizmos.DrawLine(end, end - new Vector3(0, 0, Const.MACRO_CHUNK_WIDTH_IN_METERS));
            
        }
        if (macroChunk.Data.RoadData != null)
            DrawRoadGizmos(level, basePosition);

#if UNITY_EDITOR
        if (Settings.DrawMacroChunkPosition == level)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = chunkBorderColor;

            var p = (transform.position +
                     new Vector3(Const.MACRO_CHUNK_WIDTH_IN_METERS * 0.4f, 0, Const.MACRO_CHUNK_WIDTH_IN_METERS * 0.45f));
            Handles.Label(p, macroChunk.Pos.ToString(), style);
        }
#endif
    }

    private void DrawRoadGizmos(SettingsLevel level, Vector3 basePosition)
    {

        if (Settings.DrawHighwayEdges == level)
        {
            Gizmos.color = new Color(137/256f, 19 / 256f, 93 / 256f, 1f);

            var nodes = macroChunk.Data.RoadData.MacroRoadNodes;

            foreach (var node in nodes.Values)
            {
                foreach (var neighbour in node.Neighbours)
                {
                    Gizmos.DrawLine(basePosition + node.GlobalPos, basePosition + neighbour);
                }
            }

        }


        if (Settings.DrawHighwaySplines == level)// && macroChunk.Pos == new Point2(6, -2))
        {
            Gizmos.color = roadColor;

            var roadSegments = macroChunk.Data.RoadData.IntersectingRoadSegments;
            if (roadSegments != null && roadSegments.Count > 0)
            {
                foreach (var segment in roadSegments)
                {

                    for (int i = 0; i < segment.Spline.Count - 1; i++)
                    {
                        var v1 = segment.Spline[i];
                        var v2 = segment.Spline[i + 1];
                        Gizmos.DrawLine(basePosition + v1, basePosition + v2);


                        var dir = (v2 - v1).normalized;
                        var right = new Vector3(dir.z, 0, -dir.x);

                        for (int j = -15; j <= 15; j++)
                        {
                            Gizmos.DrawLine(basePosition + v1 + right * j * 0.5f, basePosition + v2 + right * j * 0.5f);
                        }
                    }

                }
            }
        }

        if (Settings.DrawHighwayOutlines == level)
        {
            var segments = macroChunk.Data.RoadData.IntersectingRoadSegments;
            foreach (var segment in segments)
            {
                if (segment.RoadOutline == null)
                    continue;

                Gizmos.color = new Color(173 / 256f, 8 / 256f, 61 / 256f, 1f);
                for (int i = 0; i < segment.RoadVertices.Count - 1; i++)
                {
                    var v1 = segment.RoadVertices[i];
                    var v2 = segment.RoadVertices[i + 1];
                    Gizmos.DrawLine(basePosition + v1.x.ToVec3(), basePosition + v2.x.ToVec3());
                    Gizmos.DrawLine(basePosition + v2.z.ToVec3(), basePosition + v2.x.ToVec3());
                    Gizmos.DrawLine(basePosition + v2.z.ToVec3(), basePosition + v1.z.ToVec3());
                    Gizmos.DrawLine(basePosition + v1.x.ToVec3(), basePosition + v1.z.ToVec3());
                }


                Gizmos.color = new Color(237 / 256f, 130 / 256f, 1 / 256f, 1f);
                for (int i = 0; i < segment.ShoulderVertices.Count - 1; i++)
                {
                    var v1 = segment.ShoulderVertices[i];
                    var v2 = segment.ShoulderVertices[i + 1];

                    var r1 = segment.RoadVertices[i];
                    var r2 = segment.RoadVertices[i + 1];

                    Gizmos.DrawLine(basePosition + v1.x.ToVec3(), basePosition + v2.x.ToVec3());
                    Gizmos.DrawLine(basePosition + v2.z.ToVec3(), basePosition + v1.z.ToVec3());


                    Gizmos.DrawLine(basePosition + v1.z.ToVec3(), basePosition + r1.z.ToVec3());
                    Gizmos.DrawLine(basePosition + v1.x.ToVec3(), basePosition + r1.x.ToVec3());
                    Gizmos.DrawLine(basePosition + v2.z.ToVec3(), basePosition + r2.z.ToVec3());
                    Gizmos.DrawLine(basePosition + v2.x.ToVec3(), basePosition + r2.x.ToVec3());
                }
            }
        }
    }
}
