﻿using System;
using System.Collections.Generic;
using ClipperLib;
using UnityEngine;

public class RoadSegment : IEquatable<RoadSegment> {

    public Vector3 V1;
    public Vector3 V2;

    public List<Vector3> Spline { get; private set; }

    /// <summary>
    /// List of spline segment left and right side vertices after they've been extruded to match the width of the road
    /// Note that this list might be shorter than the actual number of spline elements, because the last spline segments might not intersect
    /// with the chunk that this roadsegment is a part of and therefore are not needed.
    /// </summary>
    public List<Pair<IntPoint>> RoadVertices { get; private set; }

    /// <summary>
    /// List of spline segment left and right side vertices after they've been extruded to match the width of the road + its shoulder
    /// Note that this list might be shorter than the actual number of spline elements, because the last spline segments might not intersect
    /// with the chunk that this roadsegment is a part of and therefore are not needed.
    /// </summary>
    public List<Pair<IntPoint>> ShoulderVertices { get; private set; }

    /// <summary>
    /// Spline segment bouding boxes, length of Spline.Count - 1. Element i defines the bounding box of segment i - i+1 of road
    /// </summary>
    public List<Rect> RoadRects { get; private set; }

    /// <summary>
    /// Spline segment bouding boxes, length of Spline.Count - 1. Element i defines the bounding box of segment i - i+1 of road + its shoulder
    /// </summary>
    public List<Rect> ShoulderRects { get; private set; }

    public List<IntPoint> RoadOutline { get; private set; }

    public List<IntPoint> ShoulderOutline { get; private set; }

    public float Radius { get; private set; }

    public RoadSegment(Vector3 v1, Vector3 v2, List<Vector3> spline, float radius) {
        V1 = v1;
        V2 = v2;
        Spline = spline;
        Radius = radius;
    }

    public void SetRoadAndShoulderVertices(List<Pair<IntPoint>> roadVertices, List<Pair<IntPoint>> shoulderVertices)
    {
        RoadVertices = roadVertices;
        ShoulderVertices = shoulderVertices;
        
        CalculateSplineRects();

        RoadOutline = new List<IntPoint>();
        ShoulderOutline = new List<IntPoint>();


        for (int i = 0; i < roadVertices.Count; i++)
        {
            RoadOutline.Add(roadVertices[i].x);
            if (i < ShoulderVertices.Count - 1)
            {
                ShoulderOutline.Add(shoulderVertices[i].x);
            }
        }

        for (int i = roadVertices.Count - 1; i >= 0; i--)
        {
            RoadOutline.Add(roadVertices[i].z);
            if (i < ShoulderVertices.Count - 1)
            {
                ShoulderOutline.Add(shoulderVertices[i].z);
            }
        }
    }

    private void CalculateSplineRects()
    {
        RoadRects = new List<Rect>();
        ShoulderRects = new List<Rect>();
        for (int i = 0; i < RoadVertices.Count - 1; i++)
        {
            var v1r = RoadVertices[i];
            var v2r = RoadVertices[i + 1];
            var points = new List<Vector3>() {v1r.x.ToVec3(), v1r.z.ToVec3(), v2r.x.ToVec3(), v2r.z.ToVec3() };
            RoadRects.Add(ChunkUtil.GetPointsRect(points));

            if (i < ShoulderVertices.Count - 1)
            {
                var v1s = ShoulderVertices[i];
                var v2s = ShoulderVertices[i + 1];
                points = new List<Vector3>() {v1s.x.ToVec3(), v1s.z.ToVec3(), v2s.x.ToVec3(), v2s.z.ToVec3()};
                ShoulderRects.Add(ChunkUtil.GetPointsRect(points));
            }
        }
    }

    /// <summary>
    /// Reverses the "direction" of the road segment data by switching V1, V2 and reversing the of spline collection order
    /// </summary>
    /// <returns>the new spline</returns>
    public List<Vector3> Reverse()
    {
        var temp = V1;
        V1 = V2;
        V2 = temp;
        
        Spline.Reverse();

        return Spline;
    }

    public Vector3 Other(Vector3 v)
    {
        if (V1 == v)
        {
            return V2;
        }
        else
        {
            return V1;
        }
    }
    
    public bool Equals(RoadSegment other)
    {
        return other != null && ((other.V1 == V1 && other.V2 == V2) || (other.V2 == V1 && other.V1 == V2));
    }


    public override int GetHashCode()
    {
        var hashCode = 352033288;
        hashCode = hashCode*-1521134295*V1.GetHashCode() + hashCode * -1521134295 * V2.GetHashCode();
        return hashCode;
    }

}
